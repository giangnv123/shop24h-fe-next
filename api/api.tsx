export const API_PRODUCTS = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/products'
export const API_POSTS = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/posts'
export const API_VOUCHERS = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/vouchers'
export const API_SIGNUP = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/signup'
export const API_LOGIN = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/login'
export const API_USER_LOGIN = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/user-login'
export const API_USER = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/users'
export const API_ORDER = 'https://shop24handrewdev.onrender.com/api/v1/shop24h/orders'

// https://shop24handrewdev.onrender.com/
export const API_LOCATION = `https://provinces.open-api.vn/api/?depth=3`;

