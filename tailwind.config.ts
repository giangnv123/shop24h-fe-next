import type { Config } from "tailwindcss"

const config = {
  darkMode: ["class"],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
  ],
  prefix: "",
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      colors: {
        primary: '#38bdf8'
      },

    },
    screens: {
      'xs': '350px',
      'sm': '476px',
      'md': '789px',
      'lg': '987px',
      'xl': '1200px',
      '2xl': '1536px',
    },
  },
  plugins: [require("tailwindcss-animate")],
} satisfies Config

export default config