'use client'
import { API_PRODUCTS } from '@/api/api';
import Container from '@/components/layout/Container';
import HeadingText from '@/components/shared/HeadingText';
import { LoaderRipple } from '@/components/shared/Loader';
import FilterItem from '@/components/products/FilterItem';
import { ProductProp } from '@/props/props';
import { useInfiniteQuery } from '@tanstack/react-query';
import axios from 'axios';
import Search from '@/components/shared/Search';
import React, { FC, useEffect, useState } from 'react'
import { useInView } from 'react-intersection-observer';
import { ChevronDownIcon, SearchXIcon } from 'lucide-react';
import PriceFilter from '@/components/PriceFilter';
import { cn } from '@/lib/utils';
import ProductList from '@/components/products/ProductList';

interface pageProps {
    searchParams: any
}

const fetchProductsScroll = async ({ pageParam, q = '', attribute, type, gender, max }:
    {
        pageParam: number;
        q: string;
        attribute: string;
        type: string;
        gender: string;
        max: string
    }) => {
    try {
        const res = await axios.
            get(`${API_PRODUCTS}?page=${pageParam}&q=${q}&attribute=${attribute}&type=${type}&gender=${gender}&max=${max}`)
        return res.data.data as ProductProp[]
    } catch (error) {
        console.log(error, ' ERROR');
    }
}

const ProductPage: FC<pageProps> = ({ searchParams }) => {
    const [showFilter, setShowFilter] = useState<boolean>(false)
    const [products, setProducts] = useState<any | undefined>(undefined)

    const q = searchParams?.q || ''
    const attribute = searchParams?.attribute || ''
    const type = searchParams?.type || ''
    const gender = searchParams?.gender || ''
    const max = searchParams?.max || ''

    const { ref, inView } = useInView();

    const { data, status, error, fetchNextPage, isFetchingNextPage, hasNextPage } = useInfiniteQuery({
        queryKey: ['posts', q, attribute, type, gender, max],
        queryFn: ({ pageParam = 1 }) => fetchProductsScroll({
            pageParam,
            q,
            attribute,
            type,
            gender,
            max
        }),
        initialPageParam: 1,
        getNextPageParam: (lastPage, allPages) => {
            const isNextPage = lastPage?.length !== 0
            return isNextPage ? allPages.length + 1 : undefined
        }
    })

    useEffect(() => {
        if (data) {
            data.pages && setProducts(data.pages.flat())
        }
    }, [data])


    useEffect(() => {
        if (inView && hasNextPage)
            fetchNextPage()
    }, [inView, hasNextPage, fetchNextPage])

    return (
        <Container className='pb-20 min-h-screen max-w-[1400px] md:px-10'>
            <HeadingText>Our Latest Products</HeadingText>

            {status === 'pending' && !products?.length && <div className='py-20 flex justify-center'><LoaderRipple /></div>}

            {status === 'error' &&
                <div className='p-10 text-lg'>Something went wrong! Please try again
                    <br /> {error.message}</div>}

            {(status === 'success' || products) &&
                <div className='flex gap-x-6 lg:mt-20 mt-10 lg:flex-row flex-col lg:items-start'>
                    <div className='mb-6 lg:hidden block md:text-lg font-bold'>
                        <button onClick={() => setShowFilter(show => !show)}
                            className=' text-slate-700  rounded-sm flex gap-3 items-center
                        py-1 px-2 md:px-5 bg-slate-200 border-2 border-slate-300  dark:bg-slate-100 
                        '>
                            Filter Product <ChevronDownIcon />
                        </button>
                    </div>
                    <div className={cn('lg:w-[275px] duration-500 overflow-hidden lg:block flex flex-col transition-all gap-x-8 gap-y-3',
                        showFilter ? 'max-h-[1000px]' : 'max-h-0 lg:max-h-[1000px]')}>
                        <div className='flex flex-col lg:gap-2 mb-2 md:mb-6 w-full'>
                            <h4 className='md:text-xl text-orange-500 uppercase font-bold mb-2'>Search</h4>
                            <Search className='max-w-md' pagination={false} placeHolder='Find Product' />
                        </div>
                        <div className='flex flex-col lg:gap-2 mb-2 md:mb-6 w-full'>
                            <h4 className='md:text-xl text-orange-500 uppercase font-bold mb-2'>Price</h4>
                            <PriceFilter />

                        </div>
                        <div className='flex lg:flex-col sm:flex-row flex-col gap-x-4 sm:gap-x-6'>
                            {filterItems.map(filter => {
                                return <div className='flex flex-col gap-2 mb-2 md:mb-6 w-full' key={filter.properties}>
                                    <h4 className='md:text-xl text-orange-500 uppercase font-bold '>{filter.properties}</h4>
                                    {filter.values.map(value =>
                                        <FilterItem key={value} properties={filter.properties}
                                            valueFilter={value.replaceAll(' ', '-')} />)}
                                </div>
                            })}
                        </div>
                    </div>
                    {!products?.length ? <div className='text-lg font-semibold w-full text-center flex flex-col justify-center items-center'>
                        <SearchXIcon className='w-24 h-24' />
                        <h4 className='my-2'>Cannot Find Any Product</h4>
                        <p>Please Try with different keyword or filter</p>
                    </div> :
                        <ProductList products={products} />}
                </div>}
            <div className='py-6 flex justify-center w-full' ref={ref}>
                {isFetchingNextPage && <LoaderRipple />}
            </div>

        </Container>
    )
}

const filterItems = [
    {
        properties: 'attribute',
        values: [
            'urbas', 'basas', 'vintas', 'track 6', 'pattas'
        ]
    },
    {
        properties: 'type',
        values: [
            'low top', 'high top', 'mule'
        ]
    },

    {
        properties: 'gender',
        values: [
            'male', 'female'
        ]
    },

]


export default ProductPage;
