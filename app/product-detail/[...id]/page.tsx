'use client'

import { GetProductContext } from '@/components/context/ProductContext';
import Container from '@/components/layout/Container';
import DetailView from '@/components/product-detail/DetailView';
import OverView from '@/components/product-detail/OverView';
import RelatedProducts from '@/components/product-detail/RelatedProducts';

import NoItemFound from '@/components/shared/NoItemFound';
import { ProductProp } from '@/props/props';
import Link from 'next/link';
import React, { FC } from 'react'

interface pageProps {
    params: { id: string }
}

const ProductDetail: FC<pageProps> = ({ params }) => {

    const products = GetProductContext() as ProductProp[]

    const productDetailInfo = products.find(product => product._id === params.id[0]);

    const relatedProducts = shuffleProducts(products.filter(product => product._id !== params.id).slice(0, 12));

    if (!productDetailInfo) {
        return <Container className='py-10 lg:py-14 lg:px-5'><NoItemFound /></Container>;
    }

    return (
        <Container className='py-10 lg:py-14 lg:px-5'>
            <div className=''>
                <div className='hidden md:flex items-center gap-3 capitalize'>
                    <Link href={'/products'}>Products</Link>
                    <div className='w-[1px] h-4 bg-slate-400 dark:bg-slate-300'></div>
                    <Link href={`/products?attribute=${productDetailInfo.properties.attribute}`}>{productDetailInfo.properties.attribute}</Link>
                    <div className='w-[1px] h-4 bg-slate-400 dark:bg-slate-300'></div>
                    <h2 className='font-bold'>{productDetailInfo.title} - {productDetailInfo.properties.type}</h2>
                </div>
                <div className='h-[2px] w-full bg-black mt-2 dark:bg-slate-400'></div>
                <div className='mt-4 grid lg:grid-cols-2 grid-cols-1 gap-y-4'>
                    <OverView product={productDetailInfo} />
                    <DetailView product={productDetailInfo} />
                </div>

                <RelatedProducts products={relatedProducts} />
            </div>
        </Container>
    )
}

function shuffleProducts(array: ProductProp[]) {
    return array.slice().sort(() => Math.random() - 0.5);
}

export default ProductDetail;