'use client'
import { GetUserContext } from '@/components/context/UserContext';
import UserForm from '@/components/user/UserForm';
import UserLayout from '@/components/user/UserLayout';
import { LoginSignUpIndicator } from '@/components/utilComponentJsx';
import React from 'react'

const UserPage = () => {
    const { user, handleUpdateUser } = GetUserContext();

    if (!user) return <LoginSignUpIndicator />

    return (
        <UserLayout user={user}>
            <UserForm handleUpdateUser={handleUpdateUser} user={user} />
        </UserLayout>
    )
}

export default UserPage;