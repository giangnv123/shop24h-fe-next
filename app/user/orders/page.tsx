'use client'
import { GetUserContext } from '@/components/context/UserContext';
import UserLayout from '@/components/user/UserLayout';
import UserOrder from '@/components/user/UserOrder';
import { LoginSignUpIndicator } from '@/components/utilComponentJsx';
import React from 'react'

const UserOrderPage = () => {
    const { user } = GetUserContext();

    if (!user) return <LoginSignUpIndicator />

    return (
        <UserLayout user={user}>
            <UserOrder user={user} />
        </UserLayout>
    )
}

export default UserOrderPage;