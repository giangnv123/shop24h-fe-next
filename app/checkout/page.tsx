'use client'

import CheckoutContent from '@/components/checkout/CheckoutContent';
import { GetCartContext } from '@/components/context/CartContext';
import { GetUserContext } from '@/components/context/UserContext';
import Container from '@/components/layout/Container';
import { LoginSigupForm } from '@/components/shared/FormLoginSignUp';
import NoProductInCart from '@/components/shared/NoProductInCart';
import { Button } from '@/components/ui/button';
import { ChevronRight } from 'lucide-react';
import Image from 'next/image';
import Link from 'next/link';
import React, { useState } from 'react'


const CheckOutPage = () => {
    const { user } = GetUserContext()
    const { cartProducts } = GetCartContext()
    const [orderCode, setOrderCode] = useState<string>('')

    const hanldeSetOrderCode = (value: string) => {
        setOrderCode(value)
    }

    if (!user) {
        return (
            <div className='flex min-h-[80vh] justify-center py-20'>
                <div className='p-5 lg:p-10 border rounded-md shadow-md '>
                    <LoginSigupForm />
                </div>
            </div>
        )
    }

    if (orderCode) {
        return (
            <Container className='min-h-[70vh] grid lg:grid-cols-2 gap-8 p-4'>
                <div className='grid place-items-center'>
                    <Image width={500} height={500} alt='thank you' className='w-full h-auto max-w-xs lg:max-w-md' src={'/thank-you.png'} />
                </div>
                <div className='flex flex-col items-center justify-center text-center space-y-4'>
                    <h1 className='text-4xl lg:text-5xl font-bold mb-4'>Thank You For Your Order</h1>
                    <p className='max-w-prose text-lg'>Your purchase is much more than a transaction. It&apos;s the beginning of a relationship we truly value. Thank you for choosing us.</p>
                    <p className='text-base font-medium'>We hope to see you again soon!</p>
                    <p className='text-xl font-semibold'>Your order code is: <span className='text-2xl text-blue-600'>{orderCode}</span></p>
                    <Link href={'/user/orders'}>
                        <Button >View Order Details</Button>
                    </Link>
                </div>
            </Container>
        );
    }

    return (
        <div className='py-10'>
            {cartProducts.length ?
                <Container className=' dark:bg-slate-900 lg:pb-20 pb-10 lg:px-5 xl:px-10' >
                    <div className=' mb-4 lg:mb-6'>
                        <h1 className='font-mono text-3xl text-orange-500'>WalkWell</h1>
                        <div className='hidden sm:flex items-center gap-1.5 text-sm font-semibold mt-2'>
                            <p>Information</p>
                            <ChevronRight className='h-4 w-4' />
                            <p>Shipping</p>
                            <ChevronRight className='h-4 w-4' />
                            <p>Payment</p>
                        </div>
                    </div>
                    <CheckoutContent hanldeSetOrderCode={hanldeSetOrderCode} user={user} />
                </Container>
                :
                <Container className='lg:min-h-[70vh]'>
                    <NoProductInCart />
                </Container>
            }
        </div>

    )
}

export default CheckOutPage;