'use client'

import Hero from "@/components/homepage/Hero";
import { CarousalProduct } from "@/components/homepage/Carousal";
import Policy from "@/components/homepage/Policy";

export default function Home() {

  return (
    <>
      <Hero />
      <Policy />
      <CarousalProduct />
    </>
  )
}
