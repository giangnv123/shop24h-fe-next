'use client'
import Container from '@/components/layout/Container';
import React, { FC } from 'react'

interface pageProps {
    searchParams: any
}


const ContactPage: FC<pageProps> = ({ }) => {
    return (
        <Container className='py-20'>
            <h1 className='text-3xl font-semibold mb-6'>Contact Page</h1>
        </Container>
    )
}

export default ContactPage;
