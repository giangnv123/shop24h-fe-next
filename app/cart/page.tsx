'use client'

import React from 'react';
import Link from 'next/link';
import { GetCartContext } from '@/components/context/CartContext';
import ProductCartItem from '@/components/cart/ProductCartItem';
import SummaryCard from '@/components/cart/SummaryCard';
import Container from '@/components/layout/Container';
import { Button } from '@/components/ui/button';
import NoProductInCart from '@/components/shared/NoProductInCart';

const CartPage = () => {
    const { cartProducts } = GetCartContext() || [];

    return (
        <Container className='lg:py-14 py-10 flex gap-10 lg:px-10 px-2 lg:flex-row flex-col'>
            {cartProducts.length > 0 ? (
                <>
                    <div className='flex-1'>
                        <h4 className='bg-slate-100 font-bold text-xl py-1 px-2 dark:text-slate-600'>
                            Your Cart ({cartProducts.length})
                        </h4>
                        <div className='flex flex-col sm:gap-6 gap-4 mt-6'>
                            {cartProducts.map((product, i) => (
                                <ProductCartItem last={i === cartProducts.length - 1} key={product._id + product.size} product={product} />
                            ))}
                        </div>
                        <div className='bg-black h-[1px] mt-8 dark:bg-slate-300'></div>
                        <div className='mt-5'>
                            <Link href='/products'>
                                <Button className='bg-slate-50 rounded-none text-lg' variant='link'>Continue Shopping</Button>
                            </Link>
                        </div>
                    </div>
                    <div className='lg:w-[300px] xl:w-[350px]'>
                        <SummaryCard />
                    </div>
                </>
            ) : (
                <NoProductInCart />
            )}
        </Container>
    );
};

export default CartPage;
