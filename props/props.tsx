import { AddressProps } from "@/components/checkout/component/propsUtilCheckout";
import { ProductInCartProps } from "@/components/context/CartContext";

export interface ProductProp {
    title: string;
    description: string;
    price: number;
    salePrice: number;
    _id: string;
    images: string[];
    properties: {
        [key: string]: string
    };
    category: string;
    inventories: InventoryProps[];
    code: string
}

export interface InventoryProps {
    [key: string]: number
}

export interface OrderDetailProps {
    _id: string,
    orderCode: string,
    user: string,
    createdAt: string,
    status: string,
    clientInfo: {
        fullname: string,
        phone: string,
        email: string,
    }
    shippingAddress: {
        city: string,
        district: string,
        ward: string,
        address: string,
    },
    paymentMethod: string,
    items: ProductInCartProps[],
    totalPrice: number,
    discount: number,
    updateNewAddress?: boolean,
    isPaid: boolean;

}

export interface UserProps {
    username: string,
    email: string,
    photo: string,
    phone: string,
    fullname: string,
    _id: string,
    role: string,
    cart: ProductInCartProps[],
    address: AddressProps,
    orders: OrderDetailProps[],
}

export interface UserUpdatedProps {
    username?: string,
    phone?: string,
    email?: string,
    photo?: string,
    fullname?: string,
    cart?: ProductInCartProps[],
    role?: string,
    _id?: string,
    address?: AddressProps,
}
