import React, { FC } from 'react'
import Container from '../layout/Container';
import Link from 'next/link';
import { Button } from '../ui/button';

interface HeroProps {

}

const Hero: FC<HeroProps> = ({ }) => {
    return (
        <>
            <Container>
                <div className="py-20 mx-auto text-center flex items-center flex-col max-w-3xl" >
                    <h1 className="text-4xl tracking-tight font-bold  sm:text-6xl">
                        Welcome to Our Shoe Store
                        <span className="text-primary"> WalkWell</span>
                    </h1>
                    <p className="mt-6 text-lg max-w-prose text-muted-foreground">
                        For everyday casual wear, comfort is key. Look for versatile options like sneakers,
                        loafers, or casual boots that can be paired with a variety of outfits.
                    </p>
                    <div className="flex flex-col sm:items-stretch items-center sm:flex-row gap-4 mt-6">
                        <Link href='/products'>
                            <Button>Browse Trending</Button>
                        </Link>
                        <Button variant={'ghost'}>Our quality promise &rarr; </Button>
                    </div>
                </div>
            </Container>
            <div className='hr mb-20'></div>
        </>

    )
}

export default Hero;