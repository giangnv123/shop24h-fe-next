'use client'

import { useMemo } from "react";
import * as React from "react"

import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
} from "@/components/ui/carousel"
import { GetProductContext } from "../context/ProductContext";
import Container from "../layout/Container";
import Image from "next/image";
import { Button } from "../ui/button";
import Link from "next/link";
import HeadingText from "../shared/HeadingText";

export function CarousalProduct() {
    const data = GetProductContext()

    const filterData = useMemo(() => data.filter(item => item.description.length > 150).slice(0, 6), [data])

    return (
        <Container className="overflow-hidden mb-10">
            <HeadingText>Our Trending Products</HeadingText>
            <Carousel className="w-full mx-auto md:py-20 py-10">
                <CarouselContent className="">
                    {filterData.map((product, index) => (
                        <CarouselItem key={index}>
                            <div className="grid gap-x-10 gap-y-6 lg:grid-cols-2 grid-cols-1 sm:px-10">
                                <div className="flex flex-col gap-3 justify-center lg:items-start items-center columns-2">
                                    <h1 className="text-2xl sm:text-4xl capitalize font-semibold text-center lg:text-start ">
                                        {product.title}
                                    </h1>
                                    <p className="text-lg max-w-prose text-center lg:text-start">{descriptionRendering(product.description)}</p>
                                    <Link href={`/product-detail/${product._id}`}>
                                        <Button className="rounded-none" variant={'primary'}>View Details</Button>
                                    </Link>
                                </div>
                                <div className="grid place-items-center columns-1">
                                    <Image src={product.images[0]} alt={product.title} width={500} height={500} />
                                </div>
                            </div>

                        </CarouselItem>
                    ))}
                </CarouselContent>
                <CarouselPrevious className="sm:flex hidden" />
                <CarouselNext className="sm:flex hidden" />
            </Carousel>
            <div className="my-10 flex justify-center ">
                <Link href={'/products'}>
                    <Button className="rounded-none" size={'lg'}>See All Products</Button>
                </Link>
            </div>
        </Container>
    )
}

const descriptionRendering = (text: string) => {
    return text.split(' ').length > 40 ? text.split(' ').slice(0, 40).join(' ') + '...' : text

}