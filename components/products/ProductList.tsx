import React, { FC } from 'react';
import { memo } from 'react';
import ProductCard from './ProductCard';
import { ProductProp } from '@/props/props';

interface ProductListProps {
    products: ProductProp[];
}

const ProductList: FC<ProductListProps> = ({ products }) => {
    // Filter out duplicate products based on their _id
    const uniqueProducts = products.reduce((unique, product) => {
        if (!unique.some(p => p._id === product._id)) {
            unique.push(product);
        }
        return unique;
    }, [] as ProductProp[]);

    if (!uniqueProducts.length) {
        return <h1>Something went wrong</h1>;
    }

    return (
        <div className='flex-1 grid lg:grid-cols-3 grid-cols-2 md:gap-x-6 gap-x-2 md:gap-y-4 gap-y-2'>
            {uniqueProducts.map((product: ProductProp) => <ProductCard key={product._id} product={product} />)}
        </div>
    );
}

export default memo(ProductList);
