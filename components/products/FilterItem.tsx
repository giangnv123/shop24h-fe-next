'use client'

import { cn } from "@/lib/utils";
import { X } from "lucide-react";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { memo, useCallback, useEffect, useMemo, useState } from "react";

function FilterItem({ valueFilter, properties }: { valueFilter: string, properties: string }) {
    const [checked, setChecked] = useState<boolean>(false)
    const { replace } = useRouter()
    const pathname = usePathname()
    const searchParams = useSearchParams()

    const targetProperties = useMemo(() => searchParams.get(properties), [properties, searchParams])

    useEffect(() => {
        // Check if the filter should be set as checked based on the URL search params on mount
        if (targetProperties) {
            const values = targetProperties.split(',');
            if (values && values.includes(valueFilter)) {
                setChecked(true);
            }
        }
    }, [targetProperties, valueFilter]);

    useEffect(() => {
        searchParams.get('q') && setChecked(false)
    }, [searchParams])

    const handleClick = useCallback(() => {
        const params = new URLSearchParams(searchParams)
        const values = params.get(properties)?.split(',') || []

        params.delete('q')

        if (checked) {
            params.set(properties, values.filter(v => v !== valueFilter).join(','))
        } else {
            params.set(properties, [...values, valueFilter].join(','))
        }

        params.forEach((value, key) => {
            if (!value) params.delete(key)
        })

        replace(`${pathname}?${params.toString().replaceAll('%2C', ',')}`, { scroll: false })

        setChecked(prev => !prev)
    }, [checked, pathname, properties, replace, searchParams, valueFilter])

    return (
        <button onClick={handleClick}
            className={cn(" hover:cursor-pointer dark:hover:bg-slate-50/10 hover:bg-slate-200/70 flex justify-between items-center py-2 pl-4 sm:pl-1 lg:pl-5 pr-2",
                checked && 'bg-slate-200/80 font-bold dark:bg-slate-50/20')}>
            <span className={('block capitalize md:text-lg')} >
                {valueFilter.replaceAll('-', ' ')}
            </span>
            {checked && <X className="h-6 w-6 text-red-400" />}
        </button>
    )
}

export default memo(FilterItem)


