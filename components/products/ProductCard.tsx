import { ProductProp } from "@/props/props";
import Image from "next/image";
import Link from "next/link";
import { memo } from "react";
import { Button } from "../ui/button";
import { calInventory } from "../UtilFunction";

function ProductCard({ product }: { product: ProductProp }) {
    const inventory = calInventory(product.inventories);
    const isSoldOut = inventory === 0;

    const productContent = (
        <div className={`relative capitalize group ${isSoldOut ? 'pointer-events-none' : ''}`}>
            <div className='h-[30vw] lg:h-[17.5vw] relative'>
                <Image priority className='group-hover:md:opacity-0 absolute top-0 left-0 h-full object-cover w-full'
                    src={product.images[0]} alt={product.title} width={800} height={800} />
                {isSoldOut && <div className="absolute top-0 left-0 h-full w-full flex justify-center items-center">
                    <span className="text-2xl text-white font-bold bg-black bg-opacity-50 py-2 px-4 rounded">Sold Out</span>
                </div>}
                <Image priority className='hidden md:block group-hover:opacity-100 opacity-0 absolute top-0 left-0 h-full object-cover w-full'
                    src={product.images[1]} alt={product.title} width={800} height={800} />
                <div className='md:flex hidden justify-center z-5 absolute bottom-0 left-0 w-full opacity-0 group-hover:opacity-100 transition-all'>
                    <Button className='rounded-none hover:bg-orange-500/90 bg-orange-600 uppercase w-[80%] h-12'
                        disabled={isSoldOut}>Buy Now</Button>
                </div>
            </div>
            <p className='text-orange-500 absolute top-3 right-2 rotate-12 sm:text-base group-hover:text-xl text-sm'>{product.properties.attribute}</p>

            <p className='text-orange-500 pb-1 border-b border-orange-500 self-stretch border-dashed text-center mt-1'>New Arrival</p>

            <div className='flex flex-col py-2 sm:px-3 px-1 items-center text-center sm:text-base text-sm'>
                <h2 className='font-bold'>{product.title}</h2>
                <p>{product.properties.color}</p>
                <p className='font-bold mt-1'>{product.price.toLocaleString()} VND</p>
            </div>
        </div>
    );

    return isSoldOut ? productContent : <Link href={`/product-detail/${product._id}`}>{productContent}</Link>;
}

export default memo(ProductCard);
