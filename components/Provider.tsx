'use client'
import React, { FC } from 'react'
import AppLayout from './layout/AppLayout';
import { QueryClientProvider, QueryClient } from '@tanstack/react-query'
import CartContextProvider from './context/CartContext';
import UserContextProvider from './context/UserContext';
import { EdgeStoreProvider } from '../lib/edgestore';
const queryClient = new QueryClient()


interface ProviderProps {
    children: React.ReactNode
}

const Provider: FC<ProviderProps> = ({ children }) => {
    return (
        <QueryClientProvider client={queryClient}>
            <EdgeStoreProvider>
                <UserContextProvider>
                    <CartContextProvider>
                        <AppLayout>{children}</AppLayout>
                    </CartContextProvider>
                </UserContextProvider>
            </EdgeStoreProvider>
        </QueryClientProvider>
    )
}

export default Provider;