import { API_LOCATION, API_PRODUCTS, API_USER_LOGIN } from "@/api/api"
import { ProductProp } from "@/props/props"
import axios from "axios"
import { checkResOkay } from "../UtilFunction"

export const fetchProducts = async () => {
    try {
        const res = await axios.get(API_PRODUCTS)
        return res.data.data as ProductProp[] || ''
    } catch (error) {
        console.log(error, ' -- Error fetching products')
    }
}

export const fetchProductsScroll = async ({ pageParam }: { pageParam: number }) => {
    try {
        const res = await axios.get(`${API_PRODUCTS}?page=${pageParam}`)
        return res.data.data as ProductProp[]
    } catch (error) {
        console.log(error, ' ERROR');
    }
}

export const fetchLocation = async () => {
    try {
        const res = await axios.get(API_LOCATION);
        return res.data || [];
    } catch (error) {
        console.log(error, ' ERROR');
    }
}

export const fetchUser = async () => {
    const token = localStorage.getItem('clientToken');
    if (token) {
        try {
            const response = await axios.get(API_USER_LOGIN, {
                headers: { Authorization: `Bearer ${token}` }
            });

            if (checkResOkay(response.status)) {
                return response.data.user
            }
            throw new Error('Fail to login');
        } catch (error) {
            localStorage.removeItem('clientToken');
            console.clear()
            return null
        }
    } else {
        return null
    }
}
