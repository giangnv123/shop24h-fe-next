/* eslint-disable @next/next/no-img-element */
'use client'
import { AlignJustify, MoonStarIcon, ShoppingBagIcon, Sun, X } from 'lucide-react';
import React, { FC, useEffect, useRef, useState } from 'react'
import Container from './Container';
import Link from 'next/link';
import { cn } from '@/lib/utils';
import { useOnClickOutside } from '@/components/hooks/use-on-click-outside';
import { Cart } from '../shared/CartSheet';
import { GetCartContext } from '../context/CartContext';
import { LoginSignUpDialog } from '../shared/LogInSignUpDialog';
import { GetUserContext } from '../context/UserContext';
import { DropDownAvatar } from '../shared/DropDownAvatar';
import { handleLogOut } from '../UtilFunction';


interface HeaderProps {
    toggleDarkMode: () => void;
    isDark: boolean
}

const Header: FC<HeaderProps> = ({ toggleDarkMode, isDark }) => {

    const { user } = GetUserContext()

    const { cartProducts } = GetCartContext()

    const [showNav, setShowNav] = useState<boolean>(false)

    const toggleNav = () => {
        setShowNav(!showNav)
    }

    useEffect(() => {
        const handler = (e: KeyboardEvent) => {
            if (e.key === 'Escape') {
                setShowNav(false)
            }
        }
        document.addEventListener('keydown', handler as unknown as EventListener)

        return () => {
            document.removeEventListener('keydown', handler as unknown as EventListener)
        }
    }, []);

    const navRef = useRef<HTMLDivElement | null>(null)
    useOnClickOutside(navRef, () => setShowNav(false))


    return (
        <>
            <div className='h-20 shadow-md flex items-center sticky top-0 bg-white dark:bg-slate-900 z-40'>
                <Container className='flex w-full justify-between items-center max-w-7xl lg:px-5'>
                    <div className='lg:ml-0 ml-6'>
                        <Link href={'/'}>
                            <img className='h-14' alt='logo' src='https://ananas.vn/wp-content/themes/ananas/fe-assets/images/svg/Logo_Ananas_Header.svg' />
                        </Link>
                    </div>
                    <ul className='hidden lg:flex items-center gap-1'>
                        <Link className='link-nav' href='/'>Home</Link>
                        <Link className='link-nav' href='/products'>Products</Link>
                        {/* <Link className='link-nav' href='/blogs'>Blogs</Link> */}
                        <Link className='link-nav' href='/contact'>Contact Us</Link>
                    </ul>
                    <div className='flex items-center gap-2'>

                        {!user && <div className='hidden lg:flex'><LoginSignUpDialog /></div>}
                        <div className='hidden lg:flex gap-2 items-center'>
                            <button onClick={toggleDarkMode} className={cn('btn-icon text-primary')}>
                                {isDark ? <Sun /> : <MoonStarIcon className='w-6 h-6 ' />}
                            </button>
                            <div className='slash'></div>
                            <Cart />
                        </div>

                        <button onClick={toggleNav} className='lg:hidden btn-icon'>
                            <AlignJustify />
                        </button>
                        {user && <>
                            <div className='slash lg:block hidden'></div>
                            <div className='lg:block hidden'>
                                <DropDownAvatar user={user} />
                            </div>

                        </>}
                    </div>
                </Container>
            </div>
            <div ref={navRef}
                className={cn('fixed top-2 shadow-md w-80 sm:max-w-[90%] max-w-[80%] bg-white p-4 rounded-md z-40 -right-full dark:bg-slate-900 border dark:border-slate-600',
                    showNav ? 'right-2 duration-500' : '')}>
                <div className='absolute top-3 right-2'>
                    <button onClick={() => setShowNav(!showNav)} className='btn-icon '>
                        <X />
                    </button>
                </div>
                <ul onClick={() => setShowNav(false)} className='flex gap-1 lg:hidden flex-col items-start'>
                    {!user && <LoginSignUpDialog />
                    }
                    <Link className='link-nav' href='/'>Home</Link>
                    <Link className='link-nav' href='/products'>Products</Link>
                    <Link className='link-nav' href='/cart'>About Us</Link>
                    <Link className='link-nav flex gap-2' href='/cart'>Cart
                        <button className='flex gap-1 items-center'>
                            <ShoppingBagIcon />({cartProducts.length})</button> </Link>
                    {user &&
                        <>
                            <Link className='link-nav flex gap-2' href={'/user'}>Account</Link>
                            <button onClick={handleLogOut} className='link-nav flex gap-2'>Log out</button>
                        </>
                    }
                </ul>
                <div className='hr mb-5 mt-2'></div>
                <div className='flex justify-between items-center'>
                    <span>Swtich Theme</span>
                    <div className='flex items-center gap-2'>
                        <span>{isDark ? 'Light' : 'Dark'}</span>
                        <button onClick={toggleDarkMode} className={cn('btn-icon text-primary')}>
                            {isDark ? <Sun /> : <MoonStarIcon className='w-6 h-6 ' />}
                        </button>
                    </div>
                </div>
            </div>
            <div className={cn('fixed top-0 left-0 w-full h-screen backdrop-blur-sm z-20', showNav ? 'block' : 'hidden')}></div>
        </>

    )
}

export default Header;