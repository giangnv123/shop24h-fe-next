/* eslint-disable @next/next/no-img-element */
import React, { FC } from 'react'
import Container from './Container';
import { BookOpen, FacebookIcon, HomeIcon, InstagramIcon, MailIcon, PhoneIcon, Send, TwitterIcon, YoutubeIcon } from 'lucide-react';
import { useRouter } from 'next/navigation';

interface FooterProps {

}

const Footer: FC<FooterProps> = ({ }) => {
    const router = useRouter()
    return (
        <>
            <div className='bg-slate-900 md:py-20 py-28 px-5 sm:px-10 text-slate-300 border-t border-slate-700  '>
                <Container className='flex sm:justify-around gap-x-12 gap-y-6 flex-wrap'>
                    <div className=''>
                        <div className='flex items-center gap-2'>
                            <img className='h-16' alt='logo' src='https://ananas.vn/wp-content/themes/ananas/fe-assets/images/svg/Logo_Ananas_Header.svg' />
                            <span className='text-2xl font-semibold text-slate-200'>WalkWell</span>
                        </div>
                        <p className='text-slate-300 mt-3' >Let Us Make You Special!</p>
                    </div>

                    <div>
                        <h1 className='text-xl font-semibold mb-3'>About Us</h1>
                        <ul className='flex flex-col gap-2'>
                            <li className='flex gap-2 p-1'><MailIcon /> Email: XxLpW@example.com</li>
                            <li className='flex gap-2 p-1'><PhoneIcon /> Hotline: 0222448699</li>
                            <li className='flex gap-2 p-1'><HomeIcon /> Address: Ho Chi Minh</li>
                            <li className='flex gap-2 p-1'><BookOpen /> Term and condition</li>
                        </ul>
                    </div>
                    <div>
                        <h1 className='ml-1 text-xl font-semibold mb-3'>Products</h1>
                        <ul className='flex flex-col gap-2'>
                            <li onClick={() => router.push('/products?attribute=basas')} className='footer-link'>Basas</li>
                            <li onClick={() => router.push('/products?attribute=vintas')} className='footer-link'>Vintas</li>
                            <li onClick={() => router.push('/products?attribute=urbas')} className='footer-link'>Urbas</li>
                            <li onClick={() => router.push('/products?attribute=track-6')} className='footer-link'>Track 6</li>
                        </ul>
                    </div>
                    <div>
                        <h1 className='text-xl font-semibold mb-3'>Get In Touch</h1>
                        <div className='flex gap-4'>
                            <TwitterIcon className='hover:text-primary cursor-pointer transition-all h-8 w-8' />
                            <FacebookIcon className='hover:text-primary cursor-pointer transition-all h-8 w-8' />
                            <InstagramIcon className='hover:text-primary cursor-pointer transition-all h-8 w-8' />
                        </div>
                        <h2 className='mt-5'>Send Us Your Email</h2>
                        <div className='max-w-[95%] relative' >
                            <input className='w-full py-2 px-3 bg-slate-200 outline-none border-none text-slate-900' />
                            <div className='cursor-pointer absolute top-1/2 -translate-y-1/2 
                            right-0 bg-orange-500 h-full px-2 grid place-items-center'>
                                <Send className=' text-slate-200 ' />

                            </div>
                        </div>
                    </div>
                    <div className='h-[1px] w-full bg-slate-700' />
                    <p className='max-w-prose text-center '>
                        &copy; Created By Andrew Nguyen 2023. All rights reserved.
                    </p>
                </Container>
            </div>
        </>

    )
}

export default Footer;