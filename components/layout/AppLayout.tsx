'use client'
import { useQuery } from "@tanstack/react-query";
import React, { FC, useCallback, useEffect, useState } from 'react'
import { Inter } from 'next/font/google'
import { Toaster } from "@/components/ui/sonner"
import Header from '@/components/layout/Header'
const inter = Inter({ subsets: ['latin'] })
import Footer from '@/components/layout/Footer'
import { LoaderRipple } from '../shared/Loader'
import { fetchProducts } from "../hooks/fetch";
import { ProductContext } from "../context/ProductContext";
import { cn } from "@/lib/utils";
import { GetUserContext } from "../context/UserContext";
import Image from "next/image";


interface appProps {
    children: React.ReactNode
}

const bodyStyle = 'transition-colors duration-500 min-h-screen relative text-slate-600 dark:text-slate-300 bg-white dark:bg-slate-900'

const AppLayout: FC<appProps> = ({ children }) => {
    const { isLoadingUser } = GetUserContext()

    const [isDarkMode, setIsDarkMode] = useState<boolean>(false)

    const toggleDarkMode = useCallback(() => {
        setIsDarkMode(prevMode => !prevMode);
        localStorage.setItem('isDarkMode', JSON.stringify(!isDarkMode));
    }, [isDarkMode]);

    useEffect(() => {
        (() => {
            const getTheme = localStorage.getItem('isDarkMode') === 'true'
            setIsDarkMode(getTheme)
        })()
    }, [])

    const { data, isLoading, error } = useQuery({
        queryKey: ['products'],
        queryFn: fetchProducts
    })

    return (
        <html className={cn('', isDarkMode ? 'dark' : '')} lang="en">
            <body className={cn(inter.className, bodyStyle)}>
                {(isLoading || isLoadingUser) && <div className='h-screen grid place-items-center'><LoaderRipple /></div>}

                {error && <div className="h-screen flex justify-center items-center flex-col">
                    <Image src={'/unconnected.png'} width={400} height={400} alt={'unconnected'} />
                    <h1>Something went wrong. Please try again.</h1>
                </div>}

                {!isLoading && !isLoadingUser && data &&
                    <ProductContext.Provider value={data}>
                        <Header isDark={isDarkMode} toggleDarkMode={toggleDarkMode} />

                        {children}
                        <Footer />

                    </ProductContext.Provider>
                }
                {isDarkMode ? <Toaster theme="light" richColors /> : <Toaster richColors />}

            </body>

        </html>
    )
}

export default AppLayout;