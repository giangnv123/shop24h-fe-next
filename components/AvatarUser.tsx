import React, { FC } from 'react'
import { memo } from 'react'
import { Avatar, AvatarFallback, AvatarImage } from './ui/avatar';
import { GetUserContext } from './context/UserContext';
interface AvatarUserProps {
    className?: string
}

const AvatarUser: FC<AvatarUserProps> = ({ className }) => {
    const { user } = GetUserContext()

    const avatarRender = user?.photo || '/noavatar.png'

    return (
        <Avatar className={className}>
            <AvatarImage className='object-cover' src={avatarRender} />
            <AvatarFallback>CN</AvatarFallback>
        </Avatar>
    )
}

export default memo(AvatarUser);