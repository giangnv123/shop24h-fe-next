'use client'

import Image from 'next/image';
import React, { FC } from 'react'
import { memo } from 'react'
import { GetCartContext, ProductInCartProps } from '../context/CartContext';
import { Trash2Icon } from 'lucide-react';
import SizeQuanPicker from './SizeQuanPicker';
import Link from 'next/link';
import { cn } from '@/lib/utils';

interface ProductCartItemProps {
    product: ProductInCartProps,
    last: boolean,
    smallCart?: boolean
}

const ProductCartItem: FC<ProductCartItemProps> = ({ product, last, smallCart = false }) => {
    const { deleteProduct } = GetCartContext();

    const handleDeleteProduct = () => {
        deleteProduct(product);
    }
    return (
        <div>
            <div className={cn('flex gap-2 sm:gap-3',)}>
                <Link href={'/product-detail/' + product._id}   >
                    <div className='sm:w-auto w-[35vw]'>
                        <Image priority className='h-full' src={product.image} alt={product.title} width={150} height={150} />
                    </div>
                </Link>
                <div className={cn('flex flex-col xs:gap-3 md:mr-auto self-stretch flex-1')}>
                    <h1 className='max-w-[175px] sm:max-w-[250px] leading-4 sm:leading-5 font-bold text-slate-800 capitalize dark:text-slate-200'>{product.title} - {product.type}</h1>
                    <h4 className='text-sm font-semibold text-slate-500 dark:text-slate-200'>
                        <span className='font-bold text-slate-500 dark:text-inherit'>Price: </span>
                        {product.price.toLocaleString()} VND</h4>
                    <div className='mt-auto sm:min-w-[300px] w-full'>
                        <SizeQuanPicker product={product} />
                    </div>
                </div>
                <div className={cn('hidden md:flex flex-col gap-3 items-end', smallCart && 'md:hidden')}>
                    <h4 className='font-bold lg:text-xl sm:text-lg text-orange-500 text-right'>
                        {(product.price * product.quantity).toLocaleString()} VND
                    </h4>
                    <h5 className='font-semibold text-orange-500'>In Stock</h5>
                    <button onClick={handleDeleteProduct}
                        className='bg-slate-800 dark:bg-slate-200 dark:text-slate-800 border-none shadow-sm py-2
                                px-10 text-white flex justify-center mt-auto'>
                        <Trash2Icon />
                    </button>
                </div>

            </div>
            <div className={cn('md:hidden flex justify-between w-full mt-2', smallCart && 'md:flex')}>
                <h5 className='font-semibold text-orange-500 text-lg'>In Stock</h5>
                <div className='flex flex-col gap-1'>
                    <h4 className='font-bold lg:text-xl sm:text-lg text-orange-500 text-right'>
                        {(product.price * product.quantity).toLocaleString()} VND
                    </h4>
                    <button onClick={handleDeleteProduct}
                        className='bg-slate-800 dark:bg-slate-200 dark:text-slate-800 border-none shadow-sm py-1
                                  px-3 text-white flex justify-center mt-auto'>
                        <Trash2Icon className='h-5 w-5' />
                    </button>
                </div>
            </div>
            {!last && <div className='h-[1px] bg-slate-300 dark:bg-slate-700 w-full sm:mt-6 mt-4'></div>}

        </div>
    )
}


export default memo(ProductCartItem);