import React, { FC, useMemo } from 'react';
import { GetCartContext } from '@/components/context/CartContext';
import { GetUserContext } from '../context/UserContext';
import { Button } from '../ui/button';
import VoucherComponent from '../shared/VoucherInput';
import { LoginSignUpDialog } from '../shared/LogInSignUpDialog';
import { useRouter } from 'next/navigation';

const SummaryCard: FC = () => {
    const router = useRouter();
    const { user } = GetUserContext();
    const { cartProducts, totalPrice, feeDelivery } = GetCartContext();

    const totalQuantity = useMemo(() => cartProducts.reduce((total, product) => total + product.quantity, 0), [cartProducts]);

    const deliveryInfo = feeDelivery > 0 ? (
        <p className="text-xs">Orders greater than 1,000,000 VND qualify for free delivery.</p>
    ) : (
        <p className="text-green-600 text-xs">You have qualified for free delivery!</p>
    );

    const checkoutButton = user ? (
        <Button onClick={() => router.push('/checkout')} className='rounded-none px-4 xs:w-auto w-full' size={'lg'} variant={'orange'}>
            Checkout
        </Button>
    ) : (
        <LoginSignUpDialog checkout={true} />
    );

    return (
        <div className="bg-white dark:bg-slate-800 p-2 sm:p-4 shadow-sm border dark:border-slate-600">
            <h3 className="text-lg font-semibold border-b pb-3 dark:border-slate-600 mb-5">Summary</h3>
            <VoucherComponent />
            <div className="my-4">
                <p className="font-medium text-gray-700 dark:text-gray-300">Total Products: <span className="font-bold">{totalQuantity}</span></p>
            </div>
            <div className="my-4">
                <p className="font-medium text-gray-700 dark:text-gray-300">Total Price: <span className="font-bold">{totalPrice.toLocaleString()} VND</span></p>
            </div>
            <div className="my-4">
                <p className="font-medium text-gray-700 dark:text-gray-300">Delivery Charge: <span className="font-bold">{feeDelivery.toLocaleString()} VND</span></p>
                {deliveryInfo}
            </div>
            <div className="flex justify-between items-center mt-6 xs:flex-nowrap flex-wrap gap-2">
                <span className="text-xl font-bold dark:text-white">{(totalPrice + feeDelivery).toLocaleString()} VND</span>
                {checkoutButton}
            </div>
        </div>
    );
};

export default SummaryCard;
