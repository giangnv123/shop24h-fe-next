import React, { FC, useRef, useState, useMemo, memo } from 'react'
import { ChevronDownIcon } from 'lucide-react'
import { useOnClickOutside } from '../hooks/use-on-click-outside'
import { GetCartContext, ProductInCartProps } from '../context/CartContext'
import { cn } from '@/lib/utils'

interface SizeQuantityProps {
    product: ProductInCartProps
}

const menuStyle = 'min-w-[175px] bg-white gap-1 z-50 grid grid-cols-4 py-3 px-2 rounded-md w-full shadow-md absolute opacity-0 top-0 sm:left-0 right-0 invisible'
const itemBtnStyle = 'border border-slate-600 py-1 text-sm font-semibold dark:text-black'
const btnTriggerStyle = 'sm:min-w-[100px] w-full dark:text-black border border-black dark:bg-slate-100 font-bold flex justify-between items-center py-1 px-2'

const SizeQuanPicker: FC<SizeQuantityProps> = ({ product }) => {
    const { changeProductSize, changeQuantity } = GetCartContext()

    const [showSize, setShowSize] = useState<boolean>(false)
    const [showQuan, setShowQuan] = useState<boolean>(false)

    const sizeBoxRef = useRef<HTMLDivElement | null>(null)
    const quanBoxRef = useRef<HTMLDivElement | null>(null)

    useOnClickOutside(sizeBoxRef, () => setShowSize(false))
    useOnClickOutside(quanBoxRef, () => setShowQuan(false))

    const totalQuan = useMemo(() => {
        return product.inventories.find(inventory => inventory.size === product.size)?.quantity
    }, [product.size, product.inventories])

    const handleChooseSize = (value: number | string) => {
        changeProductSize(product._id, value)
        setShowSize(false)
    }

    const handleChooseQuan = (newQuantity: number) => {
        changeQuantity(product._id, product.size, newQuantity)
        setShowQuan(false)
    }

    const renderQuantityOptions = () => {
        return Array.from({ length: Math.min(totalQuan || 0, 12) }, (_, i) => (
            <button className={itemBtnStyle}
                key={i + 1} onClick={() => handleChooseQuan(i + 1)}>{i + 1}</button>
        ))
    }

    return (
        <div className='grid grid-cols-2 gap-x-2 sm:gap-x-4'>
            {/* Size Picker */}
            <div>
                <h4 className='capitalize font-bold'>Size</h4>
                <div ref={sizeBoxRef} className='relative'>
                    <button onClick={() => setShowSize(show => !show)} className={btnTriggerStyle} aria-haspopup="true" aria-expanded={showSize}>
                        {product.size}
                        <ChevronDownIcon className={cn('transition-all', showSize && 'rotate-180')} />
                    </button>
                    <div className={cn(menuStyle, showSize && 'opacity-100 visible top-full transition-all')}>
                        {product.inventories.map(inventory =>
                            <button onClick={() => handleChooseSize(inventory.size)}
                                disabled={inventory.quantity === 0 || inventory.size === product.size}
                                className={cn(itemBtnStyle, inventory.quantity === 0 && 'text-slate-300 cursor-not-allowed dark:text-slate-400')}
                                key={inventory.size}>{inventory.size}</button>
                        )}
                    </div>
                </div>
            </div>

            {/* Quantity Picker */}
            <div>
                <h4 className='capitalize font-bold'>Quantity</h4>
                <div ref={quanBoxRef} className='relative'>
                    <button disabled={!product.size} onClick={() => setShowQuan(show => !show)}
                        className={btnTriggerStyle} aria-haspopup="true" aria-expanded={showQuan}>
                        {product.quantity}
                        <ChevronDownIcon />
                    </button>
                    <div className={cn(menuStyle, showQuan && 'opacity-100 visible top-full transition-all')}>
                        {renderQuantityOptions()}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default memo(SizeQuanPicker)
