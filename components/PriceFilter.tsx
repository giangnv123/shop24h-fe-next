'use client'

import { memo, useCallback, useEffect } from 'react';
import { ArrowRightCircleIcon } from 'lucide-react';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useState } from 'react'
import { GetProductContext } from './context/ProductContext';
import { toast } from "sonner"


const styleInput = 'w-full py-1 pl-2 pr-1 border dark:border-slate-300  dark:bg-slate-50/10'

const PriceFilter = () => {
    const products = GetProductContext()
    // Find Lowest Price
    const minPrice = products.map(product => product.price).sort((a, b) => a - b)[0]

    const [maxPrice, setMaxPrice] = useState<number>(0)

    const searchParams = useSearchParams()
    const { replace } = useRouter()
    const pathname = usePathname()

    const handleFilterPrice = useCallback(() => {
        const params = new URLSearchParams(searchParams)

        if (!maxPrice) {
            toast.warning(
                "Check Again", {
                description: 'Please enter Your max price',
                duration: 3000
            },)
            return
        }
        if (maxPrice < minPrice) {
            toast.warning(
                "Check Again", {
                description: 'Max price must be greater than min price',
                duration: 3000,
            },)
            return
        }
        params.set('max', maxPrice.toString())
        replace(`${pathname}?${params.toString().replaceAll('%2C', ',')}`, { scroll: false })
    }, [maxPrice, minPrice, pathname, replace, searchParams])

    const handleSetMaxValue = (value: string) => {
        if (value) setMaxPrice(Number(value))
    }

    useEffect(() => {
        if (searchParams.get('max')) {
            setMaxPrice(Number(searchParams.get('max')))
        }
    }, [searchParams])

    return (
        <div className='flex gap-2 dark:text-slate-50 max-w-md '>
            <input value={minPrice.toLocaleString()} type='text' placeholder='Min' readOnly className={styleInput} />
            <input value={maxPrice} onChange={(e) => handleSetMaxValue(e.target.value)}
                min={0}
                type='number' placeholder='Max' className={styleInput} />
            <button onClick={handleFilterPrice} className='grid items-center text-slate-500 hover:text-slate-600 dark:text-slate-300
             dark:hover:text-slate-200'>
                <ArrowRightCircleIcon className='h-6 w-6' />
            </button>
        </div>
    )
}

export default memo(PriceFilter);