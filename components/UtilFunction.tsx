import { InventoryProps } from "@/props/props"

export const checkResOkay = (resStatus: number) => {
    return resStatus >= 200 && resStatus < 300
}

export const handleLogOut = () => {
    localStorage.removeItem('clientToken')
    window.location.reload()
    localStorage.removeItem('cart')
}


export const limitUserName = (user: string) => {
    if (user.length > 10) {
        return user.slice(0, 10) + '...'
    }
    return user
}

export const invalidPhoneNumber = (phone: string) => !phone.match(/^0[0-9]{9,10}$/)

export const InvalidFullName = (fullname: string) => {
    return (!fullname.trim() || fullname.trim().split(' ').length < 2)
}

export const InvalidEmail = (email: string) => !email.match(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)

export function capitalizeFirstLetterAfterSpace(input: string) {
    return input.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
}

export const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("en-GB", {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
    });
    const formattedTime = date.toLocaleTimeString("en-US", {
        hour: '2-digit',
        minute: '2-digit',
        hour12: true
    });
    return `${formattedDate} ${formattedTime}`;
};


export function calInventory(inventories: InventoryProps[]) {
    let total = 0
    inventories.forEach(inventory => {
        total += inventory.quantity
    })
    return total
}
