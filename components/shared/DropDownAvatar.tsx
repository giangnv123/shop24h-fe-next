import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuGroup,
    DropdownMenuItem,
    DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import AvatarUser from "../AvatarUser"
import { handleLogOut } from "../UtilFunction"
import Link from "next/link"
import { UserProps } from "@/props/props"

export function DropDownAvatar({ user }: { user: UserProps }) {


    return (
        <DropdownMenu>
            <DropdownMenuTrigger asChild>
                <div className='lg:flex items-center hidden gap-2 btn-icon'>
                    <AvatarUser className='h-8 w-8' />
                    <p className='text-sm'>{user.username}</p>
                </div>
            </DropdownMenuTrigger>
            <DropdownMenuContent className="w-40 bg-white font-semibold dark:bg-slate-800 dark:border-slate-600">
                <DropdownMenuGroup>
                    <Link href={'/user'}>
                        <DropdownMenuItem className="cursor-pointer hover:bg-primary/5 hover:text-primary">
                            Account
                        </DropdownMenuItem>
                    </Link>
                    <Link href={'/user/orders'}>
                        <DropdownMenuItem className="cursor-pointer hover:bg-primary/5 hover:text-primary">
                            Orders
                        </DropdownMenuItem>
                    </Link>
                </DropdownMenuGroup>
                <DropdownMenuGroup>
                </DropdownMenuGroup>
                <DropdownMenuItem onClick={handleLogOut} className="cursor-pointer hover:bg-primary/5 hover:text-primary">
                    Log out
                </DropdownMenuItem>
            </DropdownMenuContent>
        </DropdownMenu>
    )
}
