'use client'

import { cn } from '@/lib/utils';
import { SearchIcon } from 'lucide-react';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { FC, useEffect, useState } from 'react';
import { useDebouncedCallback } from "use-debounce";

interface SearchProps {
    placeHolder: string
    className?: string
    pagination?: boolean
}

const Search: FC<SearchProps> = ({ placeHolder = '', className = '', pagination = true }) => {
    // console.log('SEARCH RENDERED')
    const [searchValue, setSearchValue] = useState<string>('')

    const pathname = usePathname()
    const searchParams = useSearchParams()
    const { replace } = useRouter()

    const handleSearch = useDebouncedCallback((value: string) => {
        const params = new URLSearchParams(searchParams.toString())

        if (value.length > 0 && value.length < 3) return

        pagination ? params.set('page', '1') :
            params.forEach((_, key) => {
                if (key !== 'max') params.delete(key)
            })

        value ? params.set('q', value) : params.delete('q')

        replace(`${pathname}?${params.toString()}`)

    }, 300)

    useEffect(() => {
        const params = new URLSearchParams(searchParams.toString())
        const q = params.get('q')
        setSearchValue(q || '')
    }, [searchParams])


    return (
        <div className={cn('relative', className)}>
            <SearchIcon className={cn('absolute top-1/2 left-2 -translate-y-1/2 h-4 w-4 text-slate-600 dark:text-slate-50/50')} />
            <input value={searchValue} onChange={(e) => {
                setSearchValue(e.target.value)
                handleSearch(e.target.value)
            }}
                className='bg-slate-500/5 w-full dark:bg-slate-50/20 py-2 pl-8 pr-2 border-2 border-slate-500/30 focus:border-slate-500/70
                outline-none rounded-md focus:border-slate-700 dark:text-slate-50 dark:border-slate-500 dark:focus:border-slate-400 '
                placeholder={placeHolder} />
        </div>
    )
}

export default Search;