'use client'
import { Button } from "@/components/ui/button";
import {
    Dialog,
    DialogContent,
    DialogTrigger,
} from "@/components/ui/dialog";
import { LoginSigupForm } from './FormLoginSignUp';


export function LoginSignUpDialog({ checkout = false }: { checkout?: boolean }) {
    return (
        <Dialog>
            <DialogTrigger asChild>
                {checkout ? <Button className='rounded-none px-4 xs:w-auto w-full' size={'lg'} variant={'orange'}>
                    Checkout
                </Button> :
                    <Button className='link-nav' variant="ghost">Login</Button>
                }

            </DialogTrigger>
            <DialogContent className="rounded-lg p-6 bg-white dark:bg-slate-900 dark:text-white shadow-xl dark:border-slate-700">
                <LoginSigupForm checkout={checkout} />
            </DialogContent>
        </Dialog>
    );
}
