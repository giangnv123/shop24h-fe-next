import React, { FC } from 'react'
import { Button } from '../ui/button';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';

interface PaginationProps {
    totalPage: number
}

const Pagination: FC<PaginationProps> = ({ totalPage = 1 }) => {
    const searchParams = useSearchParams()
    const params = new URLSearchParams(searchParams.toString())
    const pathname = usePathname()
    const { replace } = useRouter()
    let page = params.get('page') || 1

    const handlePage = (index: number) => {
        const newPage = Number(page) + index
        params.set('page', newPage.toString())
        replace(`${pathname}?${params.toString()}`)
    }

    return (
        <div className='flex justify-between mt-4'>
            <Button disabled={page === '1'} onClick={() => handlePage(-1)}>Previous</Button>
            <Button disabled={page === totalPage.toString()} onClick={() => handlePage(1)}>Next</Button>
        </div>
    )
}

export default Pagination;