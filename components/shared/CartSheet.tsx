import {
    Sheet,
    SheetClose,
    SheetContent,
    SheetHeader,
    SheetTitle,
    SheetTrigger,
} from "@/components/ui/sheet"
import { ShoppingBagIcon, ShoppingCart } from "lucide-react"
import Link from "next/link"
import { Button } from "../ui/button"
import { GetCartContext } from "../context/CartContext"
import ProductCartItem from "../cart/ProductCartItem"
import { useRouter } from "next/navigation"
import { useMemo } from "react"

export function Cart() {
    const router = useRouter()
    const { cartProducts } = GetCartContext()

    // Calculate total price
    const totalPrice = useMemo(() => cartProducts.reduce((total, product) => {
        return total + (product.price * product.quantity);
    }, 0), [cartProducts])
    return (
        <Sheet>
            <SheetTrigger asChild>
                <div className="flex gap-1 cursor-pointer">
                    <button className='btn-icon'>
                        <ShoppingBagIcon />({cartProducts.length})
                    </button>
                </div>
            </SheetTrigger>
            <SheetContent className="overflow-hidden flex h-full w-full flex-col lg:max-w-lg pr-6 bg-white dark:bg-slate-900 dark:text-slate-300">
                <SheetHeader className="space-y-2.5">
                    <SheetTitle >
                        Your Cart ({cartProducts.length})
                    </SheetTitle>
                </SheetHeader>

                {cartProducts.length > 0 && (
                    <div className="whitespace-nowrap overflow-auto scrollbar-hide h-screen pb-20">
                        <div className="flex justify-between items-center text-xl  font-bold">
                            Total: <span className="text-orange-500">{totalPrice.toLocaleString()} VND</span>
                        </div>
                        <SheetClose asChild>
                            <div className="flex justify-end my-3">
                                <Button onClick={() => { router.push('/cart') }} className="rounded-none text-xl px-0 capitalize" size={'lg'} variant={'link'}>
                                    Go To Cart
                                </Button>
                            </div>
                        </SheetClose>

                        <div className='flex flex-col sm:gap-6 gap-4 mt-6'>
                            {cartProducts.length && cartProducts.map((product, i) =>
                                <ProductCartItem smallCart={true} last={i === cartProducts.length - 1} key={product._id + product.size} product={product} />)}
                        </div>


                    </div>
                )}

                {cartProducts.length === 0 && (
                    <div className="flex justify-center flex-col min-h-screen gap-6">
                        <div className="text-3xl font-semibold text-center">Your cart is empty</div>
                        <div className="flex justify-center">
                            <ShoppingCart className="w-24 h-24" />
                        </div>
                        <SheetTrigger asChild>
                            <Link className="flex justify-center" href={'/products'}>
                                <Button className="underline py-3 p-4 h-14" variant={'ghost'}>
                                    Add Items to your cart to check out
                                </Button>
                            </Link>

                        </SheetTrigger>
                    </div>
                )}


            </SheetContent>
        </Sheet>
    )
}

