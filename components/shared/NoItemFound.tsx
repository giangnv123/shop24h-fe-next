import Image from 'next/image';
import React from 'react'
import { memo } from 'react'



const NoItemFound = () => {
    return (
        <div className='flex flex-col items-center gap-2'>
            <Image className='w-auto h-auto' priority alt='No Item Found' width={400} height={400} src={'https://cdn.dribbble.com/assets/art-banners/record-d05a64c16f564e98f34e41aec5ec07d5b12eb7ea8fb3a1bb0b12250b558de93b.png'} />
            <h4 className='text-center lg:text-xl sm:text-lg font-semibold'>No Product Found</h4>
            <p className='text-center'>We do not have any product in this page</p>
        </div>
    )
}

export default memo(NoItemFound);