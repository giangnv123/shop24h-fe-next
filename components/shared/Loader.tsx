
export function LoaderRing() {
    return <div className="lds-ring "><div></div><div></div><div></div><div></div></div>
}

export function LoaderRipple() {
    return <div className="lds-ripple "><div></div><div></div></div>
}