import { cn } from '@/lib/utils'
import React, { FC } from 'react'

interface HeadingTextProps {
    children: string;
    className?: string
}

const HeadingText: FC<HeadingTextProps> = ({ children, className }) => {
    return (
        <h4 className={cn("mt-10 text-center sm:text-5xl text-3xl font-bold dark:text-slate-100", className)}>
            {children}
        </h4>
    )
}

export default HeadingText;