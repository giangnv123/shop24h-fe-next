import React, { useState, useEffect } from 'react';
import { GetCartContext } from '@/components/context/CartContext';
import axios from 'axios';
import { API_VOUCHERS } from '@/api/api';
import { toast } from 'sonner';
import { useQuery } from '@tanstack/react-query';




export interface VoucherProps {
    voucherCode: string;
    discount: number;
    maxDiscount: number;
}

const fetchAllVouchers = async () => {
    try {
        const response = await axios.get(API_VOUCHERS);
        return response.data.data || []
    } catch (error) {
        toast.error('Failed to fetch vouchers.');
    }
};

const VoucherComponent = () => {
    const {
        handleApplyVoucher,
        voucher, voucherDiscount
    } = GetCartContext();

    const { data: allVouchers } = useQuery({
        queryKey: ['vouchers'],
        queryFn: fetchAllVouchers
    })

    const [voucherValue, setVoucherValue] = useState<string>('')

    useEffect(() => {
        voucher && setVoucherValue(voucher.voucherCode)
    }, [voucher]);

    return (

        <div className=''>
            <label htmlFor="voucher"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300">Voucher Code</label>
            <div>
                <div className=" flex">
                    <input
                        type="text"
                        id="voucher"
                        name="voucher"
                        value={voucherValue}
                        onChange={(e) => setVoucherValue(e.target.value.toUpperCase())}
                        placeholder="Enter voucher code"
                        style={{ textTransform: 'uppercase' }}
                        className="mt-1 block w-full px-3 py-2 bg-white border border-r-0 border-gray-300
                 shadow-sm focus:outline-none focus:ring-orange-500 focus:border-orange-500
                 dark:bg-slate-700 dark:border-gray-600 rounded-l-sm"
                    />
                    <button
                        type="button"
                        onClick={() => handleApplyVoucher(allVouchers, voucherValue)}
                        className="mt-1 bg-orange-500 text-white px-4 py-2 border border-orange-500
                   hover:bg-orange-600 focus:outline-none focus:ring-2 focus:ring-orange-500 focus:ring-opacity-50
                   rounded-r-sm shadow-sm transition-colors"
                    >
                        Apply
                    </button>
                </div>
            </div>
            {voucherDiscount > 0 && (
                <div className="my-4">
                    <p className="font-medium text-gray-700 dark:text-gray-300">
                        Voucher Discount: <span className="text-red-500 font-bold">- {voucherDiscount.toLocaleString()} VND</span>
                    </p>
                </div>
            )}
        </div>

    );
};

export default VoucherComponent;
