import * as React from 'react';
import { useEdgeStore } from '../../lib/edgestore';
import { Button } from '@/components/ui/button';
import { GetUserContext } from '@/components/context/UserContext';
import AvatarUser from '../AvatarUser';
import Image from 'next/image';
import { XIcon } from 'lucide-react';

export default function AvatarChanger() {
    const { user, handleUpdateUser } = GetUserContext();
    const [file, setFile] = React.useState<File | null>();
    const [previewUrl, setPreviewUrl] = React.useState<string | null>(null);
    const { edgestore } = useEdgeStore();

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newFile = e.target.files?.[0];
        setFile(newFile);

        if (newFile) {
            const newPreviewUrl = URL.createObjectURL(newFile);
            setPreviewUrl(newPreviewUrl);
        }
    };

    return (
        <div className='flex mb-2'>
            <div className='flex flex-col items-center'>
                <div className='flex items-center gap-2 mb-2'>
                    <div className='relative'>
                        {previewUrl ? (
                            <Image width={120} height={120} src={previewUrl} alt="Preview" className="w-20 h-20 rounded-full object-cover" />
                        ) : (
                            <AvatarUser className='w-20 h-20 object-cover' />
                        )}
                        {previewUrl && <button onClick={() => {
                            setPreviewUrl(null)
                            setFile(null)
                        }} className='top-0 right-0 absolute h-5 w-5 bg-slate-600 flex justify-center items-center'><XIcon className='w-4 h-4 text-red-400' /></button>}
                    </div>
                </div>
                <div className='flex flex-col gap-2 items-start'>
                    {!file?.name && <label className='border rounded-sm py-1 px-2 cursor-pointer' htmlFor='file'>Upload avatar</label>}
                    <input
                        className='hidden'
                        id='file'
                        type="file"
                        onChange={handleFileChange}
                    />

                    {file?.name && (
                        <button className='border p-1 text-sm bg-primary text-white'
                            onClick={async () => {
                                if (file) {
                                    const res = await edgestore.publicFiles.upload({
                                        file,
                                        onProgressChange: (progress) => {
                                            // you can use this to show a progress bar
                                        },
                                    });

                                    // Revoke the object URL to free up resources
                                    previewUrl && URL.revokeObjectURL(previewUrl);
                                    setPreviewUrl(null);
                                    handleUpdateUser({ photo: res.url }, user?._id || '')
                                    setFile(null)
                                }
                            }}
                        >
                            Save Avatar
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
}
