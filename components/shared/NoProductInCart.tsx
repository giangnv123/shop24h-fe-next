import Image from 'next/image';
import Link from 'next/link';
import React from 'react'
import { memo } from 'react';
import { Button } from '../ui/button';



const NoProductInCart = () => {
    return (
        <div className='min-h-[400px] flex justify-center items-center w-full'>
            <div className='flex justify-center flex-col items-center gap-3 sm:text-lg text-center'>
                <Image alt='Cart' width={250} height={250} src={'/Empty-cart-new.png'} />
                <p className='font-bold '>Your shopping cart is empty!</p>
                <Link href={'/products'}>
                    <Button className='rounded-none' variant={'orange'}>Go Shopping Now</Button>
                </Link>
            </div>
        </div>
    )
}

export default memo(NoProductInCart);