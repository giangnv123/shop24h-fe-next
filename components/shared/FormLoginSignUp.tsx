import { useState } from 'react';
import { Button } from "@/components/ui/button";
import { EyeClosedIcon, EyeOpenIcon } from '@radix-ui/react-icons';
import { API_SIGNUP, API_LOGIN } from '@/api/api';
import axios from 'axios';
import { checkResOkay } from '../UtilFunction';

const initalErrorState = {
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
}

const initialFormData = {
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
}
interface ErrorProps {
    username: string,
    email: string,
    password: string,
    confirmPassword: string,
}

const inputStyle = `dark:border-slate-600 w-full px-4 py-2 border focus:border-slate-400 rounded-md dark:bg-slate-700 focus:ring-1`

export function LoginSigupForm({ checkout = false }: { checkout?: boolean }) {
    const [showPassword, setShowPassword] = useState(false);
    const [isSignUp, setIsSignUp] = useState(false);
    const [formData, setFormData] = useState(initialFormData);
    const [formErrors, setFormErrors] = useState(initalErrorState);
    const [feedback, setFeedback] = useState<string | null>(null)

    const togglePasswordVisibility = () => setShowPassword(!showPassword);

    const setError = (name: keyof typeof initalErrorState, message: string) => {
        setFormErrors(prevFormErrors => ({ ...prevFormErrors, [name]: message }));
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
        setFormErrors({ ...formErrors, [name]: '' });
    };

    const validateForm = () => {
        let errors: ErrorProps = initalErrorState;
        let formIsValid = true;
        // Simple email validation
        const emailRegex = /\S+@\S+\.\S+/;
        if (!emailRegex.test(formData.email)) {
            errors = { ...errors, email: 'Please enter a valid email address.' };
            formIsValid = false;
        }

        // Password length validation
        if (formData.password.length < 6) {
            errors = { ...errors, password: 'Password must be at least 6 characters long.' };
            formIsValid = false;
        }

        // Username special characters validation
        const usernameRegex = /^[a-zA-Z0-9_]+$/;
        if (isSignUp && !usernameRegex.test(formData.username)) {
            errors = { ...errors, username: 'Username must not contain special characters.' };
            formIsValid = false;
        }

        // Match password and confirm password for SignUp
        if (isSignUp && formData.password !== formData.confirmPassword) {
            errors = { ...errors, confirmPassword: 'Passwords do not match.' };
            formIsValid = false;
        }

        setFormErrors(errors);
        return formIsValid;
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!validateForm()) return;

        const endpoint = isSignUp ? API_SIGNUP : API_LOGIN;
        const data = isSignUp ? formData : { email: formData.email, password: formData.password };

        try {
            const res = await axios.post(endpoint, data);
            if (checkResOkay(res.status)) {
                // console.log(res.data, 'res data');

                localStorage.setItem('clientToken', `${res.data.token}`);
                window.location.reload();
            } else {
                throw new Error(res.data.message);
            }
        } catch (error: any) {
            // console.log(error.response.data.message, 'error');
            setFeedback(error.response.data.message);
            if (error.response) {
                const message = error.response.data.message;
                if (message.includes('Email')) setError('email', message);
                if (message.includes('Username')) setError('username', message);
                if (message.includes('Password')) setError('password', 'Invalid Email or Password');
            } else {
                console.error(error);
            }
        }
    };

    // Helper component to display error messages
    const ErrorMessage = ({ message }: { message: string }) => {
        return <div className="text-red-500 text-sm mt-1     dark:text-red-400">{message}</div>;
    };

    return (
        <div>
            <h4 className='text-center text-2xl font-semibold'>{isSignUp ? 'Sign Up' : 'Login'}</h4>
            <p className='text-center mb-6'>
                {checkout && !isSignUp && 'Please Login to continue'}
                {checkout && isSignUp && 'Create your account. It\'s free and only takes a minute.'}
                {!checkout && !isSignUp && 'Welcome back! Please enter your details.'}
            </p>
            {feedback && <p className='text-center -mt-2 mb-6 bg-red-100 text-red-600 py-1 px-2'>{feedback}</p>}
            <form onSubmit={handleSubmit} className="grid gap-4 w-full md:min-w-[400px]">
                {isSignUp && (
                    <div>
                        <input
                            type="text"
                            name="username"
                            value={formData.username}
                            onChange={handleChange}
                            placeholder="Username"
                            required
                            className={`  ${inputStyle}
                                            ${formErrors.username ? 'border-red-500' : ''
                                }`}
                        />
                        {formErrors.username && <ErrorMessage message={formErrors.username} />}
                    </div>
                )}
                <div>
                    <input
                        type="email"
                        name="email"
                        value={formData.email}
                        onChange={handleChange}
                        placeholder="Email"
                        required
                        className={`${inputStyle} 
                                    ${formErrors.email ? 'border-red-500' : ''
                            }`}
                    />
                    {formErrors.email && <ErrorMessage message={formErrors.email} />}
                </div>
                <div>
                    <div className="relative">
                        <input
                            type={showPassword ? 'text' : 'password'}
                            name="password"
                            value={formData.password}
                            onChange={handleChange}
                            placeholder="Password"
                            required
                            className={`${inputStyle} 
                            ${formErrors.password ? 'border-red-500' : ''
                                }`}
                        />
                        <button
                            type="button"
                            onClick={togglePasswordVisibility}
                            className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5"
                        >
                            {showPassword ? <EyeOpenIcon /> : <EyeClosedIcon />}
                        </button>
                    </div>
                    {formErrors.password && <ErrorMessage message={formErrors.password} />}

                </div>
                {isSignUp && (
                    <>
                        <div className="relative">
                            <input
                                type={showPassword ? 'text' : 'password'}
                                name="confirmPassword"
                                value={formData.confirmPassword}
                                onChange={handleChange}
                                placeholder="Password Confirm"
                                required
                                className={`${inputStyle}
                                     ${formErrors.confirmPassword ? 'border-red-500' : ''
                                    }`}
                            />
                            <button
                                type="button"
                                onClick={togglePasswordVisibility}
                                className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5"
                            >
                                {showPassword ? <EyeOpenIcon /> : <EyeClosedIcon />}
                            </button>
                        </div>
                        {formErrors.confirmPassword && <ErrorMessage message={formErrors.confirmPassword} />}
                    </>

                )}
                <Button type="submit" className="mt-4 w-full">
                    {isSignUp ? 'Create Account' : 'Login'}
                </Button>
                <button
                    type="button"
                    onClick={() => setIsSignUp(!isSignUp)}
                    className="text-sm text-blue-500 dark:text-blue-400 hover:underline mt-4 text-center"
                >
                    {isSignUp ? 'Already have an account? Login' : "Don't have an account? Sign Up"}
                </button>
            </form>
        </div>

    );
}
