import React, { FC } from 'react'
import FormInput from './component/FormInput';

interface UserDetailFormProps {
    formData: {
        fullname: string;
        phone: string;
        email: string;
        address: string
        message: string,
    };
    errors: {
        fullname: string;
        phone: string;
        email: string;
        address: string;
    };
    handleChange: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
}

const UserDetailForm: FC<UserDetailFormProps> = ({ formData, errors, handleChange }) => {

    return (
        <>
            <h2 className='text-lg font-bold uppercase border-slate-600 mb-2'>Information</h2>
            <div className='lg:grid lg:grid-cols-2 flex flex-col gap-4 lg:gap-x-4'>
                <FormInput
                    label="Fullname"
                    name="fullname"
                    value={formData.fullname}
                    onChange={handleChange}
                    error={errors.fullname}
                />
                <FormInput
                    label="Phone Number"
                    name="phone"
                    value={formData.phone}
                    onChange={handleChange}
                    error={errors.phone}
                />
                <FormInput
                    label="Email"
                    name="email"
                    type="email"
                    value={formData.email}
                    onChange={handleChange}
                    error={errors.email}
                />
                <FormInput
                    label="Address"
                    name="address"
                    type="text"
                    value={formData.address}
                    onChange={handleChange}
                    error={errors.address}
                />
                <div className='flex flex-col col-span-2'>
                    <label>Message</label>
                    <textarea
                        name='message'
                        value={formData.message}
                        onChange={(e) => handleChange(e)}
                        className='dark:border-slate-500 dark:focus:border-slate-400 dark:bg-slate-700 rounded-sm border border-slate-300 py-1 px-2 focus:border-slate-500 focus:outline-none'>
                    </textarea>
                </div>

            </div>
        </>
    );
}

export default UserDetailForm;