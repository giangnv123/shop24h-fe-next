import { ProductInCartProps } from "@/components/context/CartContext"

export interface ProvinceCityProps {
    level1_id: string,
    name: string,
    type: string,
    level2s: DistrictProps[]
}

export interface DistrictProps {
    name: string,
    type: string,
    level: string,
    level3s: WardProps[]
}

export interface WardProps {
    level3_id: string,
    name: string,
    type: string,
}


export interface OrderProps {
    fullname: string,
    phone: string,
    email: string,
    address: string,
    locationAddress: LocationAddressProps,
    updateNewAddress: boolean,
    message: string,
}

export interface OrderDetailProps {
    user: string,
    clientInfo: {
        fullname: string,
        phone: string,
        email: string,
    }
    shippingAddress: {
        city: string,
        district: string,
        ward: string,
        address: string,
    },
    paymentMethod: string,
    items: ProductInCartProps[],
    totalPrice: number,
    discount: number,
    updateNewAddress: boolean,
    message?: string,
}



export const initialAddressState = {
    address: '',
    locationAddress: {
        district: '',
        city: '',
        ward: ''
    }
}

export interface AddressProps {
    address: string,
    locationAddress: LocationAddressProps
}

export interface LocationAddressProps {
    district: string,
    city: string,
    ward: string
}

export const initialLocationAddress = {
    district: '',
    city: '',
    ward: ''
}

export const initialOrderForm = {
    fullname: '',
    phone: '',
    email: '',
    address: '',
    payment: 'COD',
    message: '',
    locationAddress: {
        district: '',
        city: '',
        ward: ''
    },
    updateNewAddress: true
}