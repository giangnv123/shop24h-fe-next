import { cn } from '@/lib/utils';
import React, { FC } from 'react'
import { memo } from 'react';

interface FormInputProps {
    label: string;
    name: string;
    value: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    error?: string;
    type?: string
}

const FormInput: React.FC<FormInputProps> = ({ label, name, value, onChange, error, type = 'text' }) => {

    return (
        <div className='flex flex-col'>
            <label>{label}</label>
            <input
                name={name}
                className={cn('dark:border-slate-500 dark:focus:border-slate-400 dark:bg-slate-700 rounded-sm border border-slate-300 py-1 px-2 focus:border-slate-500 focus:outline-none', { 'border-red-400': error })}
                value={value}
                onChange={onChange}
                type={type}
            />
            {error && <p className="text-xs text-red-600">{error}</p>}
        </div>
    )
};

export default memo(FormInput);