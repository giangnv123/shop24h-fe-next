import { CheckIcon } from 'lucide-react';
import React from 'react'
import { memo } from 'react';

const Shipping = () => {
    return (
        <>
            <h2 className='text-lg font-bold uppercase border-slate-600 mt-5 mb-2'>Shipping</h2>
            <div className="flex items-center md:gap-4 gap-2 lg:pl-4">
                <div className='w-6 h-6 border grid place-items-center'>
                    <CheckIcon className='h-6 w-6 text-green-500' />
                </div>
                <p>Standard Shipping: 2 - 5 days</p>
            </div>
        </>
    )
}

export default memo(Shipping);