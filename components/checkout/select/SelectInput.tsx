import { cn } from '@/lib/utils';
import React, { FC } from 'react'

interface SelectInputProps {
    label: string;
    name: string;
    value: string;
    onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
    options: { value: string; label: string; }[];
    disabled?: boolean;
    error?: string;
}

const SelectInput: FC<SelectInputProps> = ({ label, name, value, onChange, options, disabled, error }) => (
    <div className='flex flex-col'>
        <label className='capitalize'>{label}</label>
        <select
            name={name}
            className={cn('dark:border-slate-500 dark:focus:border-slate-400 rounded-sm border border-slate-300 py-1 px-2 focus:border-slate-500 focus:outline-none dark:bg-slate-700', { 'border-red-400': error })}
            value={value}
            onChange={onChange}
            disabled={disabled}
        >
            <option value="">Select {label}</option>
            {options.map(option => (
                <option key={option.value} value={option.value}>{option.label}</option>
            ))}
        </select>
        {error && <p className="text-xs text-red-600">{error}</p>}
    </div>
);

export default SelectInput;