import React, { useMemo } from 'react'
import { DistrictProps, ProvinceCityProps } from '../component/propsUtilCheckout';
import SelectInput from './SelectInput';
import { CheckIcon, HomeIcon } from 'lucide-react';

interface AddressSelectorProps {
    location: ProvinceCityProps[];
    selectedProvince: string;
    selectedDistrict: string;
    selectedWard: string;
    handleAddressChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
    errors: {
        city: string;
        district: string;
        ward: string;
    };
    updateAddress: boolean;
    handleUpdateAddress: () => void;
}

const AddressSelector: React.FC<AddressSelectorProps> = ({ location,
    selectedProvince,
    selectedDistrict,
    selectedWard,
    handleAddressChange,
    errors,
    updateAddress,
    handleUpdateAddress }) => {

    const districts = useMemo(() => {
        const selectedProv = location?.find((prov: ProvinceCityProps) => prov.name === selectedProvince);
        return selectedProv ? selectedProv.level2s : [];
    }, [location, selectedProvince]);

    const wards = useMemo(() => {
        const selectedDist = districts?.find((dist: DistrictProps) => dist.name === selectedDistrict);
        return selectedDist ? selectedDist.level3s : [];
    }, [districts, selectedDistrict]);

    return (
        <div className='flex flex-col gap-4 mt-4'>

            <SelectInput
                error={errors.city}
                label="Province / City"
                name="city"
                value={selectedProvince}
                onChange={handleAddressChange}
                options={location.map(province => ({ value: province.name, label: province.name }))}
            />
            <SelectInput
                error={errors.district}
                label="District"
                name="district"
                value={selectedDistrict}
                onChange={handleAddressChange}
                options={districts.map(province => ({ value: province.name, label: province.name }))}
            />
            <SelectInput
                error={errors.ward}
                label="ward"
                name="ward"
                value={selectedWard}
                onChange={handleAddressChange}
                options={wards.map(province => ({ value: province.name, label: province.name }))}
            />

            <div onClick={() => handleUpdateAddress()} className='flex items-center gap-3 cursor-pointer lg:pl-4'>
                <div className='w-6 h-6 border grid place-items-center'>
                    {updateAddress && <CheckIcon className='h-6 w-6 text-green-500' />}
                </div>
                <p >Update New Address</p>
                <HomeIcon className='text-green-500' />
            </div>
        </div>

    );
};

export default AddressSelector;