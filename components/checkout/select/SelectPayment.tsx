import { CheckIcon } from 'lucide-react';
import React, { FC } from 'react'
import { memo } from 'react';

interface SelectPaymentProps {

    selectedPaymentMethod: string
    handlePaymentMethodChange: (value: string) => void
}

const SelectPayment: FC<SelectPaymentProps> = ({ selectedPaymentMethod, handlePaymentMethodChange }) => {

    return (
        <>
            <h2 className='text-lg font-bold uppercase border-slate-600 mt-5 mb-2'>Payment</h2>
            <div className='flex flex-col gap-2 lg:pl-4'>
                <div onClick={() => handlePaymentMethodChange('COD')} className='flex items-center gap-3 cursor-pointer'>
                    <div className='w-6 h-6 border grid place-items-center'>
                        {selectedPaymentMethod === 'COD' && <CheckIcon className='h-6 w-6 text-green-500' />}
                    </div>
                    <p>Cash on Delivery (COD)</p >
                    <svg fill="#22c55e" width="50px" height="50px" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg"><path d="M15.48 12c-.13.004-.255.058-.347.152l-2.638 2.63-1.625-1.62c-.455-.474-1.19.258-.715.712l1.983 1.978c.197.197.517.197.715 0l2.995-2.987c.33-.32.087-.865-.367-.865zM.5 16h3c.277 0 .5.223.5.5s-.223.5-.5.5h-3c-.277 0-.5-.223-.5-.5s.223-.5.5-.5zm0-4h3c.277 0 .5.223.5.5s-.223.5-.5.5h-3c-.277 0-.5-.223-.5-.5s.223-.5.5-.5zm0-4h3c.277 0 .5.223.5.5s-.223.5-.5.5h-3C.223 9 0 8.777 0 8.5S.223 8 .5 8zm24 11c-1.375 0-2.5 1.125-2.5 2.5s1.125 2.5 2.5 2.5 2.5-1.125 2.5-2.5-1.125-2.5-2.5-2.5zm0 1c.834 0 1.5.666 1.5 1.5s-.666 1.5-1.5 1.5-1.5-.666-1.5-1.5.666-1.5 1.5-1.5zm-13-1C10.125 19 9 20.125 9 21.5s1.125 2.5 2.5 2.5 2.5-1.125 2.5-2.5-1.125-2.5-2.5-2.5zm0 1c.834 0 1.5.666 1.5 1.5s-.666 1.5-1.5 1.5-1.5-.666-1.5-1.5.666-1.5 1.5-1.5zm-5-14C5.678 6 5 6.678 5 7.5v11c0 .822.678 1.5 1.5 1.5h2c.676.01.676-1.01 0-1h-2c-.286 0-.5-.214-.5-.5v-11c0-.286.214-.5.5-.5h13c.286 0 .5.214.5.5V19h-5.5c-.66 0-.648 1.01 0 1h7c.66 0 .654-1 0-1H21v-9h4.227L29 15.896V18.5c0 .286-.214.5-.5.5h-1c-.654 0-.654 1 0 1h1c.822 0 1.5-.678 1.5-1.5v-2.75c0-.095-.027-.19-.078-.27l-4-6.25c-.092-.143-.25-.23-.422-.23H21V7.5c0-.822-.678-1.5-1.5-1.5z" /></svg>
                </div>
                <div onClick={() => handlePaymentMethodChange('Banking')} className='flex items-center gap-3 cursor-pointer'>
                    <div className='w-6 h-6 border grid place-items-center'>
                        {selectedPaymentMethod === 'Banking' && <CheckIcon className='h-6 w-6 text-green-500' />}
                    </div>
                    <p >Pay with bank account</p>
                    <svg fill="#22c55e" height="35px" width="35px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 508.00 508.00" xmlSpace="preserve" stroke="#000000" strokeWidth="0.00508001"><g id="SVGRepo_bgCarrier" strokeWidth="0"></g><g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <g> <path d="M97.1,423.388H80.6c-7.8,0-14.1,6.3-14.1,14.1c0,7.7,6.3,14.1,14.1,14.1h16.5c7.8,0,14.1-6.3,14.1-14.1 S104.9,423.388,97.1,423.388z"></path> </g> </g> <g> <g> <path d="M185.3,423.388h-16.5c-7.8,0-14.1,6.3-14.1,14.1c0,7.7,6.3,14.1,14.1,14.1h16.5c7.8,0,14.1-6.3,14.1-14.1 S193.1,423.388,185.3,423.388z"></path> </g> </g> <g> <g> <path d="M273.5,423.388H257c-7.8,0-14.1,6.3-14.1,14.1c0,7.7,6.3,14.1,14.1,14.1h16.5c7.8,0,14.1-6.3,14.1-14.1 S281.3,423.388,273.5,423.388z"></path> </g> </g> <g> <g> <path d="M361.7,423.388h-16.5c-7.8,0-14.1,6.3-14.1,14.1c0,7.7,6.3,14.1,14.1,14.1h16.5c7.8,0,14.1-6.3,14.1-14.1 S369.5,423.388,361.7,423.388z"></path> </g> </g> <g> <g> <path d="M496.1,175.188l-123.1-123l-39.8-39.8c-16.5-16.5-43.4-16.5-59.9,0l-181,180.7h-50c-23.3,0-42.3,19-42.3,42.3v230.3 c0,23.3,19,42.3,42.4,42.3H400c23.4,0,42.4-19,42.4-42.3v-176.9l53.8-53.7C504.1,227.088,518.3,197.288,496.1,175.188z M293.2,32.288c5.5-5.5,14.5-5.5,20,0l29.8,29.9l-131,130.9h-79.7L293.2,32.288z M400,479.788H42.4c-7.8,0-14.1-6.3-14.1-14.1 v-159.9h385.8v159.9h0C414.1,473.488,407.8,479.788,400,479.788z M414.1,277.588H28.3v-42.2c0-7.8,6.3-14.1,14.1-14.1H400 c7.8,0,14.1,6.3,14.1,14.1V277.588z M476.1,215.088l-33.8,33.8v-13.5c0-23.3-19-42.3-42.4-42.3h-148l111.1-111l113.2,113.1 C484.3,203.288,478.8,212.488,476.1,215.088z"></path> </g> </g> </g></svg>
                </div>
            </div>
        </>
    )
}

export default memo(SelectPayment);