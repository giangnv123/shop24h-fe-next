import React, { FC, memo } from 'react';
import { GetCartContext } from '../context/CartContext';
import VoucherComponent from '../shared/VoucherInput';
import ItemCardCheckOut from './component/ItemCardCheckOut';
import { Button } from '../ui/button';
import { Loader2Icon } from 'lucide-react';

interface CheckOutSummaryProps {
    handleSubmit: any;
    loadingOrder: boolean;
}

const CheckOutSummary: FC<CheckOutSummaryProps> = ({ handleSubmit, loadingOrder }) => {
    const { cartProducts, totalPrice, subTotal, feeDelivery } = GetCartContext();

    return (
        <div className='flex flex-col gap-4 dark:bg-slate-800'>
            <h2 className='text-lg font-bold pb-1 border-b uppercase border-slate-600'>Your Order</h2>
            {cartProducts.map(({ _id, size, ...rest }) =>
                <ItemCardCheckOut key={_id + size} product={{ _id, size, ...rest }} />)}
            <p className="font-medium text-gray-700 dark:text-gray-300">Subtotal Price:
                <span className="font-bold"> {subTotal.toLocaleString()} VND</span>
            </p>
            <VoucherComponent />
            <p className="font-medium text-gray-700 dark:text-gray-300">
                Delivery Charge: <span className="font-bold">{feeDelivery.toLocaleString()} VND</span>
            </p>
            {feeDelivery === 0 && (
                <p className="text-green-500 text-xs">
                    You have qualified for free delivery!
                </p>
            )}
            <div>
                <span className='font-bold text-lg'>Total: </span>
                <span className="text-xl font-bold dark:text-white">{(totalPrice + feeDelivery).toLocaleString()} VND</span>
            </div>
            <Button
                type='button'
                onClick={handleSubmit}
                disabled={loadingOrder}
                className='w-full rounded-none py-5 uppercase mt-2 flex items-center gap-2'
                variant={'orange'}>
                {loadingOrder && <Loader2Icon className='animate-spin' />}
                Place Order
            </Button>
        </div>
    );
}

export default memo(CheckOutSummary);
