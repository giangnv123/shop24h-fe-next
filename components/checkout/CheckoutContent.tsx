import React, { FC, useCallback, useEffect, useState } from 'react'
import { GetUserContext } from '../context/UserContext';
import { LocationAddressProps, OrderDetailProps, OrderProps, initialLocationAddress, initialOrderForm } from './component/propsUtilCheckout';
import { InvalidEmail, InvalidFullName, checkResOkay, invalidPhoneNumber } from '../UtilFunction';
import { GetCartContext } from '../context/CartContext';
import { memo } from 'react';
import AddressSelector from './select/AddressSelector';
import UserDetailForm from './UserDetailForm';
import Shipping from './component/Shipping';
import SelectPayment from './select/SelectPayment';
import CheckOutSummary from './CheckOutSummary';
import axios from 'axios';
import { API_ORDER } from '@/api/api';
import { toast } from 'sonner';
import { UserProps } from '@/props/props';

interface CheckoutContentProps {
    user: UserProps;
    hanldeSetOrderCode: (value: string) => void
}

const CheckoutContent: FC<CheckoutContentProps> = ({ user, hanldeSetOrderCode }) => {
    const { refetchUser } = GetUserContext()
    const { cartProducts, totalPrice, voucherDiscount, resetCart, location } = GetCartContext()
    const [loadingOrder, setLoadingOrder] = useState<boolean>(false)
    const [selectedPaymentMethod, setSelectedPaymentMethod] = useState('COD');
    const [formData, setFormData] = useState<OrderProps>({ ...initialOrderForm });
    const [errors, setErrors] = useState<OrderProps>({ ...initialOrderForm });


    // Function to handle payment method change
    const handlePaymentMethodChange = useCallback((value: string) => {
        setSelectedPaymentMethod(value);
    }, []);

    // Populate initial data from user context
    useEffect(() => {
        if (user) {
            setFormData({
                fullname: user.fullname || '',
                phone: user.phone || '',
                email: user.email || '',
                address: user.address?.address || '',
                updateNewAddress: true,
                message: '',
                locationAddress: user.address?.locationAddress || { ...initialLocationAddress }
            });
        }
    }, [user]);

    const handleAddressChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = e.target;

        setFormData(prevState => {
            let locationAddress = { ...prevState.locationAddress };
            if (name === 'city') {
                locationAddress.city = value;
                locationAddress.district = ''
                locationAddress.ward = ''
            } else if (name === 'district') {
                locationAddress.district = value;
                locationAddress.ward = ''
            } else if (name === 'ward') {
                locationAddress.ward = value;
            }
            return {
                ...prevState,
                locationAddress
            };
        })

        const errorKey = name as keyof LocationAddressProps; // Assert that name is a key of OrderProps
        if (errors['locationAddress'][errorKey]) {
            setErrors(prevState => ({
                ...prevState,
                locationAddress: {
                    ...prevState.locationAddress,
                    [errorKey]: '',
                },
            }));
        }
    };


    const handleChangeInput = useCallback((e: React.ChangeEvent<HTMLInputElement
        | HTMLTextAreaElement
        | HTMLSelectElement>) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value,
        }));
        if (errors[name as keyof OrderProps]) {
            setErrors(prevState => ({
                ...prevState,
                [name]: '',
            }));
        }
    }, [errors]);

    const validateForm = () => {
        let newErrors = {
            fullname: '',
            phone: '',
            email: '',
            address: '',
            locationAddress: {
                district: '',
                city: '',
                ward: ''
            }
        };
        let isValid = true;

        // Validation logic

        if (!formData.locationAddress.city) {
            newErrors.locationAddress.city = 'Please select city';
            isValid = false;
        }

        if (!formData.locationAddress.district) {
            newErrors.locationAddress.district = 'Please select district';
            isValid = false;
        }

        if (!formData.locationAddress.ward) {
            newErrors.locationAddress.ward = 'Please select ward';
            isValid = false;
        }

        if (InvalidFullName(formData.fullname)) {
            newErrors.fullname = 'Fullname is required';
            isValid = false;
        }

        if (!formData.phone) {
            newErrors.phone = 'Phone number is required';
            isValid = false;
        }

        if (formData.phone && invalidPhoneNumber(formData.phone)) {
            newErrors.phone = 'Please provide valid phone number';
            isValid = false;
        }

        if (!formData.email) {
            newErrors.email = 'Email is required';
            isValid = false;
        }

        if (formData.email && InvalidEmail(formData.email)) {
            newErrors.email = 'Please provide valid email';
            isValid = false;
        }

        if (!formData.address) {
            newErrors.address = 'Please provide your detail address';
            isValid = false;
        }
        // Add more validations as needed

        setErrors(prev => ({ ...prev, ...newErrors }));
        return isValid;
    };

    const toggleUpdateAddress = () => {
        setFormData(prev => ({
            ...prev,
            updateNewAddress: !prev.updateNewAddress
        }));
    }

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // console.log(formData, ' form data');

        if (!validateForm()) return

        let orderDetail: OrderDetailProps = {
            user: user._id,
            clientInfo: {
                fullname: formData.fullname,
                phone: formData.phone,
                email: formData.email,
            },
            shippingAddress: {
                city: formData.locationAddress.city,
                district: formData.locationAddress.district,
                ward: formData.locationAddress.ward,
                address: formData.address
            },
            paymentMethod: selectedPaymentMethod,
            items: cartProducts,
            totalPrice: totalPrice,
            discount: voucherDiscount,
            updateNewAddress: formData.updateNewAddress,
            message: formData.message
        }

        // handle create order

        try {
            setLoadingOrder(true)
            const res = await axios.post(API_ORDER, orderDetail)

            if (checkResOkay(res.status)) {
                toast.success('Order created successfully');
                hanldeSetOrderCode(res.data.data.order.orderCode)
                resetCart()
                window.scrollTo(0, 0);
                refetchUser()
                console.log(res.data, ' data from server')
            } else {
                throw new Error('Something went wrong')

            }
        } catch (error: any) {

            toast.error(error.response.data.message.join(' '));
        } finally {
            setLoadingOrder(false)
        }
    };

    return (
        <div className='lg:grid lg:grid-cols-5 gap-y-8 flex-col flex lg:px-0 lg:py-0 gap-x-10'>
            <div className='lg:col-span-3'>
                {/* Form Payment Infor */}
                <form className='' onSubmit={handleSubmit}>
                    <UserDetailForm formData={formData} errors={errors} handleChange={handleChangeInput} />

                    {location && <AddressSelector
                        updateAddress={formData.updateNewAddress}
                        handleUpdateAddress={toggleUpdateAddress}
                        errors={errors.locationAddress}
                        selectedWard={formData.locationAddress.ward}
                        selectedProvince={formData.locationAddress.city}
                        selectedDistrict={formData.locationAddress.district}
                        location={location}
                        handleAddressChange={handleAddressChange} />}

                    <Shipping />
                    <SelectPayment
                        selectedPaymentMethod={selectedPaymentMethod}
                        handlePaymentMethodChange={handlePaymentMethodChange} />
                </form>
            </div>
            <div className='lg:col-span-2 md:p-8 p-2 px-2 py-4 bg-slate-100 dark:bg-slate-800'>
                <CheckOutSummary loadingOrder={loadingOrder} handleSubmit={handleSubmit} />
            </div>

        </div>
    )
}

export default memo(CheckoutContent);