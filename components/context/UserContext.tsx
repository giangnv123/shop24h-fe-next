'use client'
import React, { createContext, useContext, useEffect, useState } from "react";
import axios from 'axios';
import { ProductInCartProps } from "./CartContext";
import { checkResOkay } from "../UtilFunction";
import { API_USER } from "@/api/api";
import { toast } from "sonner";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { fetchUser } from "../hooks/fetch";
import { UserProps, UserUpdatedProps } from "@/props/props";

export const updateUser = async (updatedUser: UserUpdatedProps, userId: string) => {
    try {
        const token = localStorage.getItem('clientToken');

        const response = await axios.patch(`${API_USER}/${userId}`, updatedUser, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        });

        if (checkResOkay(response.status)) {
            // console.log('User updated successfully:', response.data);
            return true
        } else {
            throw new Error('Failed to update user');
        }
    } catch (error) {

        console.error('Error updating user:', error);
        throw error; // Re-throw the error so it can be handled by the caller
    }
};

export const UserContext = createContext
    <
        {
            user: UserProps | null,
            isLoadingUser: boolean,
            handleUpdateUser: (updatedUserInfo: UserUpdatedProps, userId: string) => Promise<void>,
            updatedUserCart: (updatedCart: ProductInCartProps[]) => void,
            refetchUser: () => void
        }
        | undefined
    >(undefined);

export const GetUserContext = () => {
    const context = useContext(UserContext);
    if (context === undefined) throw new Error('UserContext must be used within a UserContextProvider');
    return context;
}

export const updateCartInDatabase = async (updatedCart: ProductInCartProps[], userId: string) => {
    try {
        const token = localStorage.getItem('clientToken');

        await axios.patch(`${API_USER}/${userId}`, { cart: updatedCart }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        });

    } catch (error) {
        console.error('Error updating cart in database:', error);
    }
};

const UserContextProvider = ({ children }: { children: React.ReactNode }) => {
    const queryClient = useQueryClient()

    const { data: userFromDatabase, isLoading } = useQuery({
        queryKey: ['user'],
        queryFn: fetchUser
    });

    const refetchUser = () => {
        queryClient.invalidateQueries({ queryKey: ['user'] })
    }

    const [user, setUser] = useState<UserProps | null>(null)

    useEffect(() => {
        if (userFromDatabase) {

            setUser(userFromDatabase)
        }
    }, [userFromDatabase])

    const updatedUserCart = (updatedCart: ProductInCartProps[]) => {
        setUser(prevUser => {
            if (prevUser) {
                return { ...prevUser, cart: updatedCart }
            }
            return prevUser
        })
    }

    const handleUpdateUser = async (updatedUserInfo: UserUpdatedProps, userId: string) => {
        try {
            const updateOk = await updateUser(updatedUserInfo, userId);
            if (updateOk) {
                const updatedUser = await fetchUser();
                setUser(updatedUser);
                toast.success('User updated successfully');
                return;
            }
            throw new Error('Failed to update user');
        } catch (error: any) {

            // Check if the error structure is as expected and if it's a MongoDB duplicate key error (error code 11000)
            if (error.response && error.response.data && error.response.data.message && error.response.data.message.keyValue) {
                const errorKey = Object.keys(error.response.data.message.keyValue)[0]; // Extract the key that caused the duplicate error

                // Display a user-friendly message
                toast.error(`The ${errorKey} is already in use, please try another!`);
            } else {
                // Handle other unexpected errors
                toast.error('An unexpected error occurred. Please try again.');
            }
            console.clear() // Clear the console
        }
    };

    const value = { user, isLoadingUser: isLoading, handleUpdateUser, updatedUserCart, refetchUser };

    return (
        <UserContext.Provider value={value}>
            {children}
        </UserContext.Provider>
    );
};

export default UserContextProvider;
