import { ProductProp } from "@/props/props";
import { createContext, useContext } from "react";

export const ProductContext = createContext<ProductProp[] | undefined>(undefined)

export const GetProductContext = () => {
    const products = useContext(ProductContext)
    if (products === undefined) throw new Error('ProductContext must be used within a ProductProvider')
    return products
}