'use client'

import { InventoryProps } from "@/props/props";
import React, { createContext, useContext, useEffect, useMemo, useState } from "react";
import { toast } from "sonner"
import { useRouter } from "next/navigation";
import { GetUserContext, updateCartInDatabase } from "./UserContext";
import { ProvinceCityProps } from "../checkout/component/propsUtilCheckout";
import { VoucherProps } from "../shared/VoucherInput";
import dataLocations from "@/location";

export const CartContext = createContext<{
    cartProducts: ProductInCartProps[],
    setCartProducts: React.Dispatch<React.SetStateAction<ProductInCartProps[]>>,
    resetCart: () => void,
    addProduct: (product: ProductInCartProps) => void,
    changeProductSize: (productId: string, newSize: number | string) => void,
    deleteProduct: (product: ProductInCartProps) => void,
    changeQuantity: (productId: string, size: string | number, newQuantity: number) => void,

    totalPrice: number,
    subTotal: number,
    setTotalPrice: (price: number) => void,
    voucher: VoucherProps | null,
    voucherDiscount: number,
    setVoucher: (voucher: VoucherProps) => void,
    handleApplyVoucher: (allVouchers: VoucherProps[], voucherCode: string) => void,
    feeDelivery: number
    location: ProvinceCityProps[]
} | undefined>(undefined);


export const GetCartContext = () => {
    const context = useContext(CartContext)
    if (context === undefined) throw new Error('CartContext must be used within a CartContextProvider')
    return context
}

export interface ProductInCartProps {
    _id: string,
    quantity: number,
    size: string | number,
    inventories: InventoryProps[],
    maxQuantity: number,
    image: string,
    title: string,
    price: number,
    type: string,
}

const CartContextProvider = ({ children }: { children: React.ReactNode }) => {
    const { user } = GetUserContext()

    const location = dataLocations

    const router = useRouter()
    const [totalPrice, setTotalPrice] = useState<number>(0);
    const [voucher, setVoucher] = useState<VoucherProps | null>(null);
    const [feeDelivery, setFeeDelivery] = useState<number>(0);
    const [cartProducts, setCartProducts] = useState<ProductInCartProps[]>([])
    const [voucherDiscount, setVoucherDiscount] = useState<number>(0);

    const resetCart = () => {
        setCartProducts([]);
        localStorage.removeItem('cart');
    }

    const subTotal = useMemo(() => cartProducts.reduce((total, product) =>
        total + product.price * product.quantity, 0), [cartProducts]);

    useEffect(() => {

        setFeeDelivery(subTotal >= 1000000 ? 0 : 25000);

        if (voucher) {
            let discountAmount = (subTotal * voucher.discount) / 100;
            if (discountAmount > voucher.maxDiscount) {
                discountAmount = voucher.maxDiscount;
            }
            setVoucherDiscount(discountAmount);
            setTotalPrice(subTotal - discountAmount);
        } else {
            setTotalPrice(subTotal);
        }
        if (user) {
            updateCartInDatabase(cartProducts, user._id);
            // updatedUserCart(cartProducts);
        }

    }, [cartProducts, voucher, user, subTotal])

    const handleApplyVoucher = (allVouchers: VoucherProps[], voucherCode: string) => {

        const foundVoucher = allVouchers.find(v => v.voucherCode === voucherCode.toLocaleLowerCase());

        if (foundVoucher) {
            setVoucher(foundVoucher);
            // // Display success message
            toast.success(`Voucher applied! ${foundVoucher.discount}% 
                            discount up to ${foundVoucher.maxDiscount.toLocaleString()} VND has been deducted.`);
        } else {
            // Reset the discount if the voucher is not valid
            setVoucher(null);
            setVoucherDiscount(0);
            toast.warning('Invalid voucher code.');
        }
    };



    const addProduct = (newProduct: ProductInCartProps) => {
        // Clone the current products array
        let updatedProducts = [...cartProducts];

        // Find index of the product if it already exists
        const existingProductIndex = updatedProducts.findIndex(item => item._id === newProduct._id && item.size === newProduct.size);

        if (existingProductIndex > -1) {
            // Calculate new quantity
            const newQuantity = updatedProducts[existingProductIndex].quantity + newProduct.quantity;

            // Check if new quantity exceeds maxQuantity
            if (newQuantity > newProduct.maxQuantity) {
                toast.warning(
                    "Exceeds maximum quantity limit", {
                    description: `This product in your cart exceed the maximum quantity limit of ${newProduct.maxQuantity}`,
                    duration: 5000
                }
                );
                return;
            }

            // Update the quantity of the existing product
            updatedProducts[existingProductIndex] = {
                ...updatedProducts[existingProductIndex],
                quantity: newQuantity
            };
        } else {
            // Product does not exist, add as new product
            updatedProducts = [...updatedProducts, newProduct];
        }

        // Set the updated products array to state
        setCartProducts(updatedProducts);

        // Update local storage
        localStorage.setItem('cart', JSON.stringify(updatedProducts));

        // Success toast message
        toast.success(
            "Product added to cart successfully", {
            duration: 4000,
            action: {
                label: 'View Cart',
                onClick: () => router.push('/cart'),
            }
        }
        );
    };

    const changeProductSize = (productId: string, newSize: number | string) => {
        // Find the index of the product that needs to be changed
        const productIndex = cartProducts.findIndex((p) => p._id === productId);
        if (productIndex === -1) {
            toast.error("Product not found in cart.");
            return;
        }

        // Clone the current product
        const productToUpdate = { ...cartProducts[productIndex] };

        // Find inventory for the new size
        const inventoryForNewSize = productToUpdate.inventories.find((inv) => inv.size === newSize);

        if (!inventoryForNewSize) {
            toast.error("Selected size is not available.");
            return;
        }

        if (inventoryForNewSize.quantity < productToUpdate.quantity) {
            toast.warning(`Only ${inventoryForNewSize.quantity} left in size ${newSize}.`, {
                description: 'Please choose a smaller quantity.',
            });
            return;
        }

        // Check if there is already a product with the same ID and the new size
        const duplicateProductIndex = cartProducts.findIndex((p) => p._id === productId && p.size === newSize);
        if (duplicateProductIndex !== -1) {
            toast.warning("A product with the selected size already exists in the cart.");
            return;
        }

        // If no duplicates, proceed to update the product size
        const updatedProducts = cartProducts.map((p, index) => {
            if (index === productIndex) {
                return {
                    ...p,
                    size: newSize,
                };
            }
            return p;
        });

        // Update the cart state and local storage
        setCartProducts(updatedProducts);
        localStorage.setItem('cart', JSON.stringify(updatedProducts));

        // Display a success message
        toast.success("Product size updated successfully.", {
            duration: 4000
        });
    };


    const deleteProduct = (deletedProduct: ProductInCartProps) => {
        // Update the cart products state to filter out the product to be deleted
        setCartProducts(currentProducts => {
            const updatedProducts = currentProducts.filter(product =>
                !(product._id === deletedProduct._id && deletedProduct.size === product.size)
            );

            // Update local storage with the new cart products array
            localStorage.setItem('cart', JSON.stringify(updatedProducts));

            // Return the new cart products array
            return updatedProducts;
        });

        // Display a success message
        toast.success("Product removed from cart successfully", {
            duration: 4000
        });
    };

    const changeQuantity = (productId: string, size: string | number, newQuantity: number) => {
        // Find the product index and its inventory for the current size
        const productIndex = cartProducts.findIndex(
            product => product._id === productId && product.size === size
        );

        // If product doesn't exist, show an error message and exit the function
        if (productIndex === -1) {
            toast.error("Product not found in cart.");
            return;
        }

        const productInventory = cartProducts[productIndex].inventories.find(
            inventory => inventory.size === size
        );

        // Check if the new quantity exceeds the available inventory
        if (productInventory && newQuantity > productInventory.quantity) {
            toast.warning(`Only ${productInventory.quantity} left in size ${size}.`, {
                description: "Please change the quantity or choose a different size.",
                duration: 4000
            });
            return;
        }

        // Construct the updated products array
        const updatedProducts = cartProducts.map((product, index) => {
            if (index === productIndex) {
                return { ...product, quantity: newQuantity };
            }
            return product;
        });

        // Now, update the state with the new products array
        setCartProducts(updatedProducts);

        // Update local storage
        localStorage.setItem('cart', JSON.stringify(updatedProducts));

        // Display a success message
        toast.success("Product quantity updated successfully.", {
            duration: 4000
        });
    };

    useEffect(() => {
        if (user && user.cart.length > 0) {
            setCartProducts(user.cart);
        } else {
            const storedCart = localStorage.getItem('cart');
            if (storedCart) {
                try {
                    const parsedCart = JSON.parse(storedCart);
                    if (user && user.cart.length === 0) {
                        updateCartInDatabase(parsedCart, user._id);
                    }
                    if (Array.isArray(parsedCart)) {
                        setCartProducts(parsedCart);
                    }
                } catch (error) {
                    console.error("Error parsing cart from local storage:", error);
                }
            }
        }


    }, [user]);

    useEffect(() => {
        const updateCart = async () => {
            if (cartProducts.length !== 0) {
                localStorage.setItem('cart', JSON.stringify(cartProducts));
            }
        };
        updateCart();
    }, [cartProducts]);

    const value = {
        setCartProducts,
        addProduct,
        changeProductSize,
        deleteProduct,
        changeQuantity,
        setTotalPrice,
        setVoucher,
        handleApplyVoucher,
        cartProducts,
        subTotal,
        totalPrice,
        voucher,
        voucherDiscount,
        feeDelivery,
        resetCart,
        location
    }

    return (
        <CartContext.Provider value={value}>
            {children}
        </CartContext.Provider>
    )
}

export default CartContextProvider;
