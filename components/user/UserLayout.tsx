'use client'
import AvatarUser from '@/components/AvatarUser';
import { limitUserName } from '@/components/UtilFunction';
import Container from '@/components/layout/Container';
import { UserProps } from '@/props/props';
import Link from 'next/link';
import React, { FC } from 'react'

interface pageProps {
    children: React.ReactNode
    user: UserProps
}

export const initialUserState = {
    username: '',
    email: '',
    phone: '',
    fullname: '',
}

const UserLayout: FC<pageProps> = ({ children, user }) => {

    return (
        <>
            <Container className='lg:py-20 py-10 lg:px-10 md:px-4 px-0  transition-none duration-0'>
                <div className="min-h-screen bg-gray-100 flex md:flex-row flex-col
                shadow-md border dark:border-slate-800 dark:bg-slate-900 ">
                    <aside className="md:w-64 lg:px-0 w-full py-10 bg-white dark:bg-slate-900">
                        <div className='flex items-center gap-2 px-3' >
                            <AvatarUser className='w-12 h-12' />
                            <h1 className="font-semibold">{limitUserName(user.username)}</h1>
                        </div>
                        <nav className="flex flex-col gap-1 mt-5 font-semibold text-lg">
                            <Link className='hover:text-orange-500 p-2 px-3 hover:bg-slate-900/10 ' href={'/user'}>Profile</Link>
                            <Link className='hover:text-orange-500 p-2 px-3 hover:bg-slate-900/10 ' href={'/user/orders'}>Orders</Link>
                        </nav>
                    </aside>
                    <main className="flex-1 md:p-7 dark:bg-slate-800">
                        {children}
                    </main>
                </div>
            </Container>
        </>
    )
}

export default UserLayout;