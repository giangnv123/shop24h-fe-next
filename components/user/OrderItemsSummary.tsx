import React, { FC } from 'react'
import { cn } from '@/lib/utils';
import { ProductInCartProps } from '../context/CartContext';
import ItemCardCheckOut from './ItemCardCheckOut';

interface OrderItemsSummaryProps {
    products: ProductInCartProps[]
}

const OrderItemsSummary: FC<OrderItemsSummaryProps> = ({ products }) => {

    if (!products?.length) return <div>No Products</div>

    return (
        <div className={cn('relative bg-slate-100 opacity-80', { editable: 'opacity-100' })}>
            <div className='flex flex-col gap-4 p-4 text-slate-600'>
                <h2 className='text-lg font-bold pb-1 border-b uppercase border-slate-600'>Order Items</h2>
                {products.map(product =>
                    <ItemCardCheckOut key={product._id + product.size} product={product} />)}
                <h3 className='text-lg -mt-2 flex gap-1'>Subtotal Price:
                    <span className='font-semibold'>
                        {(products.reduce((total, product) => total + product.price * product.quantity, 0)).toLocaleString()}
                    </span>
                </h3>
            </div>
        </div>
    )
}

export default OrderItemsSummary;