import React, { FC } from 'react'
import Image from 'next/image';
import OrderCard from './OrderCard';
import { OrderDetailProps, UserProps } from '@/props/props';

interface UserOrderProps {
    user: UserProps
}

const UserOrder: FC<UserOrderProps> = ({ user }) => {
    return (
        <div className='bg-white sm:p-3 h-full'>
            <h1 className="text-2xl font-semibold text-center mb-1">Your Order</h1>
            {user.orders.length === 0 ? (
                <div className="flex pt-10 items-center flex-col h-full text-lg font-semibold">
                    <Image className="w-64 h-64" alt="No order" width={300} height={300} src={'/no-order.png'} />
                    No Order Yet
                </div>
            ) : (
                <div>
                    <div className="px-2 sm:px-8 text-lg">
                        <h3>Total Orders: {user.orders.length} </h3>
                        <h3 className="mb-3">Total Payment: {user.orders.reduce((total: number, order: OrderDetailProps) => total + order.totalPrice, 0).toLocaleString()} VND</h3>
                    </div>

                    <div className='sm:px-4'>
                        {user.orders.map((order: OrderDetailProps) => <OrderCard key={order._id} order={order} />)}
                    </div>
                </div>
            )}
        </div>
    )
}

export default UserOrder;