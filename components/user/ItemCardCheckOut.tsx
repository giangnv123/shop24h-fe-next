import React, { FC } from 'react'
import { memo } from 'react'
import Image from 'next/image'
import { ProductInCartProps } from '../context/CartContext'

interface ItemCardCheckOutProps {
    product: ProductInCartProps
}

const ItemCardCheckOut: FC<ItemCardCheckOutProps> = ({ product }) => {
    return (
        <div className='w-full'>
            <div className='flex sm:gap-x-0 items-center' >
                <div className='relative mr-2 w-[120px]'>
                    <Image className='w-auto h-auto opacity-80' src={product.image} alt={product.title} width={200} height={200} />
                </div>
                <div className='flex flex-col mr-auto lg:max-w-[180px]'>
                    <p className='font-semibold capitalize max-w-[160px] leading-5'>
                        {product.title}
                    </p>
                    <p><span className='font-semibold'>Size:</span> {product.size}</p>
                    <p><span className='font-semibold'>Quantity:</span> {product.quantity}</p>
                </div>
                <h4 className='sm:block hidden self-center font-bold w-max text-center text-sm'>{(product.price * product.quantity).toLocaleString()} <br /> VND </h4>
            </div>
            <h4 className='sm:hidden block font-bold w-max text-right text-sm mt-1'>{(product.price * product.quantity).toLocaleString()} VND </h4>
        </div>
    )
}

export default memo(ItemCardCheckOut);