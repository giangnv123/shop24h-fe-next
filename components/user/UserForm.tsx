'use client'
import { InvalidEmail, InvalidFullName, invalidPhoneNumber } from '@/components/UtilFunction';
import AvatarChanger from '@/components/shared/AvatarChanger';
import { LoginSigupForm } from '@/components/shared/FormLoginSignUp';
import { cn } from '@/lib/utils';
import { UserProps } from '@/props/props';
import React, { FC, useEffect, useState } from 'react'
import { toast } from 'sonner';

interface pageProps {
    user: UserProps
    handleUpdateUser: (user: UserProps, userId: string) => void
}

export const initialUserState = {
    username: '',
    email: '',
    phone: '',
    fullname: '',
}

const UserForm: FC<pageProps> = ({ user, handleUpdateUser }) => {
    const [initialData, setInitialData] = useState(initialUserState);

    const [formData, setFormData] = useState(initialUserState);

    const [errors, setErrors] = useState(initialUserState);

    const isDataUnchanged = () => {
        return initialData.username === formData.username &&
            initialData.email === formData.email &&
            initialData.phone === formData.phone &&
            initialData.fullname === formData.fullname;
    };

    const validateForm = () => {
        let isValid = true;
        let errors = initialUserState;

        // Username validation (no special characters)
        if (!formData.username.match(/^[a-zA-Z0-9]+$/)) {
            errors.username = 'Username must not contain special characters.';
            isValid = false;
        }

        // Email validation
        if (InvalidEmail(formData.email)) {
            errors.email = 'Email must be a valid email email.';
            isValid = false;
        }

        if (InvalidFullName(formData.fullname)) {
            errors.fullname = 'Please Enter Your fullname';
            isValid = false;
        }

        // Phone number validation
        if (invalidPhoneNumber(formData.phone)) {
            errors.phone = 'Phone number must start with zero and be 10 to 11 digits long.';
            isValid = false;
        }

        setErrors(errors);
        console.log(errors, ' error');

        return isValid;
    };

    useEffect(() => {
        if (user) {
            const userData = {
                username: user.username || '',
                email: user.email || '',
                phone: user.phone || '',
                fullname: user.fullname || '',
            };
            setFormData(userData);
            setInitialData(userData);
        }
    }, [user]);



    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value,
        }));
        setErrors({ ...errors, [name]: '' });
    };

    if (!user) {
        return (
            <div className='flex min-h-[80vh] justify-center py-20'>
                <div className='p-10 border rounded-md shadow-md self-start'>
                    <LoginSigupForm />
                </div>
            </div>
        )
    }

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const isFormValid = validateForm();

        console.log(formData, ' form data');

        if (isFormValid) {
            await handleUpdateUser({
                ...user,
                username: formData.username,
                email: formData.email,
                phone: formData.phone,
                fullname: formData.fullname,
            }, user._id);
            // toast.success('User updated successfully');

        } else {
            toast.error('Please correct the errors before submitting.');
        }
    };
    return (
        <>

            <div className="max-w-4xl mx-auto bg-white p-3 rounded shadow dark:bg-slate-900 h-full">
                <div className='mb-6'>
                    <h1 className="text-xl font-semibold ">Edit Profile</h1>
                    <p className='text-sm border-b pb-1'>Manage and Protect your account</p>
                </div>
                {/* Avatar */}
                <AvatarChanger />

                <form onSubmit={handleSubmit}>
                    {/* Username Field */}
                    <div className="mb-6 h-full">
                        <label className="block mb-1 text-sm font-medium text-gray-900 dark:text-slate-200">Username</label>
                        <input
                            name="username"
                            type="text"
                            value={formData.username}
                            onChange={handleChange}
                            className={cn("shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm dark:bg-slate-800 dark:text-slate-200 dark:border-slate-600 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5", errors.username ? 'border-red-500' : '')}
                        />
                        {errors.username && <p className="text-xs text-red-600">{errors.username}</p>}
                    </div>

                    <div className="mb-6">
                        <label className="block mb-1 text-sm font-medium text-gray-900 dark:text-slate-200">Fullname</label>
                        <input
                            name="fullname"
                            type="text"
                            value={formData.fullname}
                            onChange={handleChange}
                            className={cn("shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm dark:bg-slate-800 dark:text-slate-200 dark:border-slate-600 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5", errors.username ? 'border-red-500' : '')}
                        />
                        {errors.fullname && <p className="text-xs text-red-600">{errors.fullname}</p>}
                    </div>

                    {/* Email Field */}
                    <div className="mb-6">
                        <label className="block mb-1 text-sm font-medium text-gray-900 dark:text-slate-200">Email</label>
                        <input
                            name="email"
                            type="email"
                            value={formData.email}
                            onChange={handleChange}
                            className={cn("shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm dark:bg-slate-800 dark:text-slate-200 dark:border-slate-600 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5", errors.email ? 'border-red-500' : '')}
                        />
                        {errors.email && <p className="text-xs text-red-600">{errors.email}</p>}
                    </div>

                    {/* Phone Number Field */}
                    <div className="mb-6">
                        <label className="block mb-1 text-sm font-medium text-gray-900 dark:text-slate-200">Phone Number</label>
                        <input
                            name="phone"
                            type="text"
                            value={formData.phone}
                            onChange={handleChange}
                            className={cn("shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm dark:bg-slate-800 dark:text-slate-200 dark:border-slate-600 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5", errors.phone ? 'border-red-500' : '')}
                        />
                        {errors.phone && <p className="text-xs text-red-600">{errors.phone}</p>}
                    </div>

                    {/* Submit Button */}
                    <button
                        type="submit"
                        className="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
                        disabled={isDataUnchanged()} // Disable button if data is unchanged
                    >
                        Save
                    </button>
                </form>
            </div>

        </>
    )
}

export default UserForm;