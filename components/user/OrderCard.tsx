import React, { FC } from 'react';
import OrderItemsSummary from './OrderItemsSummary';
import { OrderDetailProps } from '@/props/props';
import { formatDate } from '../UtilFunction';



interface OrderCardProps {
    order: OrderDetailProps;
}

const OrderCard: FC<OrderCardProps> = ({ order }) => {
    const dateTime = formatDate(order.createdAt);

    return (
        <div className='flex flex-col bg-white shadow-lg border rounded-lg p-4 mx-auto my-6'>
            <h4 className='text-md text-gray-700 mb-1'>Date: <span className='text-gray-600'>{dateTime}</span></h4>
            <h4 className='text-md text-gray-700 mb-1 mt-2 flex gap-1'>Status:
                <span className={`font-semibold ${order.status === 'Cancle' ? 'text-red-600' : order.status === 'Processing' ? 'text-blue-600' : 'text-green-600'}`}>
                    {order.status}
                </span>
            </h4>
            <h4 className='flex gap-x-2 flex-wrap'>
                Shipping to:
                {order.shippingAddress?.city && <span>{order.shippingAddress.city} - </span>}
                {order.shippingAddress?.district && <span>{order.shippingAddress.district} - </span>}
                {order.shippingAddress?.ward && <span>{order.shippingAddress.ward} - </span>}
                <span>{order.shippingAddress?.address}</span>
            </h4>

            <h4 className='text-md text-gray-700'>Total: <span className='text-green-500'>{order.totalPrice?.toLocaleString()} VND</span></h4>
            <h4 className='text-md text-gray-700 mb-2'>Discount: <span className='text-green-500'>{order.discount?.toLocaleString()} VND</span></h4>
            <OrderItemsSummary products={order.items} />
        </div>
    );
}

export default OrderCard;
