import { ProductProp } from '@/props/props';
import React, { FC, memo } from 'react'

import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
} from "@/components/ui/carousel"
import Image from 'next/image';
import HeadingText from '../shared/HeadingText';
import { useRouter } from 'next/navigation';
interface RelatedProductsProps {
    products: ProductProp[]
}

const RelatedProducts: FC<RelatedProductsProps> = ({ products }) => {
    const router = useRouter()
    return (
        <>
            <div className='hr my-10'></div>
            <HeadingText className='lg:mb-10 mb-5'>Related Product</HeadingText>
            <Carousel
                opts={{
                    align: "start",
                }}
                className="w-full"
            >
                <CarouselContent className='w-full'>
                    {products.map((product, i) => (
                        <CarouselItem key={product._id} className="md:basis-1/3 basis-1/2 lg:basis-1/4 md:pl-4 pl-3">
                            <button onClick={() => router.push(`/product-detail/${product._id}`)} className="p-1">
                                <Image priority={false} alt='product' width={400} height={400} src={product.images[0]} />
                                <div className='flex flex-col justify-center text-center gap-1 mt-1 p-2 capitalize'>
                                    <h3 className=' sm:text-lg font-bold'>{product.title}</h3>
                                    <p className='sm:text-lg '>{product.properties.color} </p>
                                    <p className='font-semibold'>{product.price.toLocaleString()} VND</p>
                                </div>
                            </button>

                        </CarouselItem>
                    ))}
                </CarouselContent>
                <CarouselPrevious className='md:-left-8 left-0 top-[40%] w-10 h-10 md:flex hidden' />
                <CarouselNext className='md:-right-5 right-0 top-[40%] w-10 h-10 md:flex hidden' />
            </Carousel>
        </>
    )
}

export default memo(RelatedProducts);