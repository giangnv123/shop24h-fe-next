/* eslint-disable @next/next/no-img-element */
import { ProductProp } from '@/props/props';
import { memo } from 'react';
import Image from 'next/image';
import React, { FC, useState } from 'react'
import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
} from "@/components/ui/carousel"
import { cn } from '@/lib/utils';
interface OverViewProps {
    product: ProductProp;
}

const OverView: FC<OverViewProps> = ({ product }) => {
    const [mainImage, setMainImage] = useState<number>(0)

    const changeImage = (index: number) => {
        setMainImage(index)
    }
    return (
        <div className='flex flex-col gap-3'>
            <Image priority={true} alt='product' width={1000} height={1000} src={product.images[mainImage]} />
            <div>
                <Carousel
                    opts={{
                        align: "start",
                    }}
                    className="w-full"
                >
                    <CarouselContent>
                        {product.images.map((url, index) => (
                            <CarouselItem onClick={() => changeImage(index)}
                                key={index} className="basis-1/4 pl-2 cursor-pointer">
                                <div className={cn("border-2", mainImage === index && 'border-orange-500')}>
                                    <Image priority={false} src={url} width={250} height={250} alt="product" />
                                </div>
                            </CarouselItem>
                        ))}
                    </CarouselContent>
                    {product.images.length > 4 && <>
                        <CarouselPrevious className="left-0 bg-black/20 text-slate-900 border-black/50" />
                        <CarouselNext className="right-0 bg-black/20 text-slate-900 border-black/50" />
                    </>}

                </Carousel>
            </div>
        </div>
    )
}

export default memo(OverView);