'use client'

import { cn } from '@/lib/utils'
import { InventoryProps, ProductProp } from '@/props/props'
import { ChevronDownIcon } from 'lucide-react'
import React, { FC, useContext, useEffect, useRef, useState } from 'react'
import { memo } from 'react'
import { useOnClickOutside } from '../hooks/use-on-click-outside'
import { Button } from '../ui/button'
import { CartContext } from '../context/CartContext'
import { toast } from "sonner"

interface SizeQuantityProps {
    inventories: InventoryProps[];
    product: ProductProp
}

const menuStyle = 'bg-white gap-1 z-50 grid grid-cols-4 py-3 px-2 rounded-md w-full shadow-md absolute opacity-0 top-0 left-0 invisible'
const itemBtnStyle = 'border border-slate-600 py-1 font-semibold dark:text-black'
const btnTriggerStyle = 'w-full dark:text-black border border-black dark:bg-slate-100 font-bold flex justify-between items-center py-1 px-2'
const SizeQuantity: FC<SizeQuantityProps> = ({ inventories, product }) => {
    const context = useContext(CartContext);

    if (!context) {
        throw new Error('SizeQuantity must be used within a CartContextProvider');
    }

    const { addProduct } = context;

    const [showSize, setShowSize] = useState<boolean>(false)
    const [sizeValue, setSizeValue] = useState<number | string | undefined>(undefined)

    const [quanValue, setQuanValue] = useState<number | undefined>(undefined)
    const [totalQuan, setTotalQuan] = useState<number | undefined>(undefined)
    const [showQuan, setShowQuan] = useState<boolean>(false)

    useEffect(() => {
        if (sizeValue) {
            const quantity = inventories.find(inventory => inventory.size === sizeValue)?.quantity
            setTotalQuan(quantity)
        }
    }, [sizeValue, inventories])

    const handleChooseSize = (value: number | string) => {
        setSizeValue(value)
        setShowSize(false)
        setQuanValue(undefined)
    }

    const handleChooseQuan = (value: number) => {
        setQuanValue(value)
        setShowQuan(false)
    }

    const sizeBoxRef = useRef<HTMLDivElement | null>(null)
    useOnClickOutside(sizeBoxRef, () => setShowSize(false))

    const quanBoxRef = useRef<HTMLDivElement | null>(null)
    useOnClickOutside(quanBoxRef, () => setShowQuan(false))

    const addProductToCart = () => {
        if (!sizeValue || !quanValue) {
            toast.warning(
                "Check Again", {
                description: 'Please Select Size and Quantity',
                // position: 'top-right',
                duration: 3000,
            },)
            return
        }
        const maxQuantity = inventories.find(inventory => inventory.size === sizeValue)?.quantity || 0

        const productAdded = {
            _id: product._id,
            maxQuantity,
            quantity: quanValue,
            size: sizeValue,
            image: product.images[0],
            title: product.title,
            price: product.price,
            type: product.properties.type,
            inventories: inventories
        }

        addProduct(productAdded)

    }


    return (
        <>
            <div className='grid sm:grid-cols-2 grid-cols-1 lg:gap-x-8 gap-x-4 gap-y-4' >
                <div>
                    <h4 className='capitalize font-bold lg:text-2xl text-lg'>Size</h4>
                    <div ref={sizeBoxRef} className='relative '>
                        <button onClick={() => setShowSize(show => !show)} className={btnTriggerStyle}>
                            {sizeValue}
                            <ChevronDownIcon className={cn('transition-all', showSize && 'rotate-180')} />
                        </button>
                        <div className={cn(menuStyle,
                            showSize && 'opacity-100 visible top-full transition-all')}>
                            {inventories.map(inventory =>
                                <button onClick={() => handleChooseSize(inventory.size)}
                                    disabled={inventory.quantity === 0 || inventory.size === sizeValue}
                                    className={cn(itemBtnStyle,
                                        inventory.quantity === 0 && 'text-slate-300 cursor-not-allowed dark:text-slate-400')}
                                    key={inventory.size}>{inventory.size}</button>
                            )}
                        </div>
                    </div>
                </div>
                <div>
                    <h4 className='capitalize font-bold lg:text-2xl text-lg'>Quantity</h4>
                    <div ref={quanBoxRef} className='relative '>
                        <button disabled={!sizeValue} onClick={() => setShowQuan(show => !show)}
                            className={btnTriggerStyle}>
                            {quanValue}
                            <ChevronDownIcon />
                        </button>
                        <div className={cn(menuStyle,
                            showQuan && 'opacity-100 visible top-full transition-all')}>
                            {totalQuan && Array.from({ length: totalQuan }).slice(0, 12).map((_, i) =>
                                <button className={itemBtnStyle}
                                    key={i + 1} onClick={() => handleChooseQuan(i + 1)}>{i + 1}</button>)}
                        </div>
                    </div>
                </div>
            </div>
            <Button onClick={() => addProductToCart()}
                variant={'orange'} className='rounded-none uppercase py-6'>Add to cart</Button>
        </>
    )
}

export default memo(SizeQuantity);