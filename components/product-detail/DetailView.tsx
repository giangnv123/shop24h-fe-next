import { ProductProp } from '@/props/props'
import React, { FC } from 'react'
import { memo } from 'react'
import SizeQuantity from './SizeQuantity'
import { calInventory } from '../UtilFunction'

interface DetailViewProps {
    product: ProductProp
}

const DetailView: FC<DetailViewProps> = ({ product }) => {
    const stock = calInventory(product.inventories)
    return (
        <div className='lg:pl-10 flex flex-col gap-6'>
            <h1
                className='text-slate-800 dark:text-slate-200 lg:text-3xl md:text-2xl text-xl font-bold uppercase'>
                {product.title} - {product.properties.type} - {product.properties.color}
            </h1>
            <p>Product Code: {product.code}</p>
            <p className='text-lg font-bold text-orange-500'>{product.price.toLocaleString()} VND</p>
            <div className='hr'></div>
            <p>{product.description}</p>
            {stock === 0 ? <p className='text-red-500'>Out of stock</p> :
                <SizeQuantity product={product} inventories={product.inventories} />
            }
        </div>
    )
}

export default memo(DetailView);