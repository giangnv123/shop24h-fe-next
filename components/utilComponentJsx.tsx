import { LoginSigupForm } from "./shared/FormLoginSignUp"

export const LoginSignUpIndicator = () => {
    return (
        <div className='flex min-h-[80vh] justify-center py-20'>
            <div className='p-10 border rounded-md shadow-md self-start'>
                <LoginSigupForm />
            </div>
        </div>
    )
}